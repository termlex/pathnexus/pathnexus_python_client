# coding: utf-8

"""
    Pathnexus

    Pathnexus - a lab data harmonisation platform  # noqa: E501

    OpenAPI spec version: 4.6.0-SNAPSHOT
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""


import pprint
import re  # noqa: F401

import six


class MapAssociation(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """

    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'created_by': 'str',
        'created_date': 'int',
        'last_modified_by': 'str',
        'last_modified_date': 'int',
        'allowed_groups': 'list[str]',
        'organisation': 'str',
        'id': 'str',
        'source_id': 'str',
        'target_id': 'str',
        'type': 'str',
        'excluded': 'bool',
        'processed': 'bool',
        'localterm': 'str',
        'target_term': 'str',
        'status': 'str',
        'code_list': 'str',
        'request': 'bool'
    }

    attribute_map = {
        'created_by': 'createdBy',
        'created_date': 'createdDate',
        'last_modified_by': 'lastModifiedBy',
        'last_modified_date': 'lastModifiedDate',
        'allowed_groups': 'allowedGroups',
        'organisation': 'organisation',
        'id': 'id',
        'source_id': 'sourceId',
        'target_id': 'targetId',
        'type': 'type',
        'excluded': 'excluded',
        'processed': 'processed',
        'localterm': 'localterm',
        'target_term': 'targetTerm',
        'status': 'status',
        'code_list': 'codeList',
        'request': 'request'
    }

    def __init__(self, created_by=None, created_date=None, last_modified_by=None, last_modified_date=None, allowed_groups=None, organisation=None, id=None, source_id=None, target_id=None, type=None, excluded=None, processed=None, localterm=None, target_term=None, status=None, code_list=None, request=None):  # noqa: E501
        """MapAssociation - a model defined in Swagger"""  # noqa: E501

        self._created_by = None
        self._created_date = None
        self._last_modified_by = None
        self._last_modified_date = None
        self._allowed_groups = None
        self._organisation = None
        self._id = None
        self._source_id = None
        self._target_id = None
        self._type = None
        self._excluded = None
        self._processed = None
        self._localterm = None
        self._target_term = None
        self._status = None
        self._code_list = None
        self._request = None
        self.discriminator = None

        if created_by is not None:
            self.created_by = created_by
        if created_date is not None:
            self.created_date = created_date
        if last_modified_by is not None:
            self.last_modified_by = last_modified_by
        if last_modified_date is not None:
            self.last_modified_date = last_modified_date
        if allowed_groups is not None:
            self.allowed_groups = allowed_groups
        if organisation is not None:
            self.organisation = organisation
        if id is not None:
            self.id = id
        if source_id is not None:
            self.source_id = source_id
        if target_id is not None:
            self.target_id = target_id
        if type is not None:
            self.type = type
        if excluded is not None:
            self.excluded = excluded
        if processed is not None:
            self.processed = processed
        if localterm is not None:
            self.localterm = localterm
        if target_term is not None:
            self.target_term = target_term
        if status is not None:
            self.status = status
        if code_list is not None:
            self.code_list = code_list
        if request is not None:
            self.request = request

    @property
    def created_by(self):
        """Gets the created_by of this MapAssociation.  # noqa: E501


        :return: The created_by of this MapAssociation.  # noqa: E501
        :rtype: str
        """
        return self._created_by

    @created_by.setter
    def created_by(self, created_by):
        """Sets the created_by of this MapAssociation.


        :param created_by: The created_by of this MapAssociation.  # noqa: E501
        :type: str
        """

        self._created_by = created_by

    @property
    def created_date(self):
        """Gets the created_date of this MapAssociation.  # noqa: E501


        :return: The created_date of this MapAssociation.  # noqa: E501
        :rtype: int
        """
        return self._created_date

    @created_date.setter
    def created_date(self, created_date):
        """Sets the created_date of this MapAssociation.


        :param created_date: The created_date of this MapAssociation.  # noqa: E501
        :type: int
        """

        self._created_date = created_date

    @property
    def last_modified_by(self):
        """Gets the last_modified_by of this MapAssociation.  # noqa: E501


        :return: The last_modified_by of this MapAssociation.  # noqa: E501
        :rtype: str
        """
        return self._last_modified_by

    @last_modified_by.setter
    def last_modified_by(self, last_modified_by):
        """Sets the last_modified_by of this MapAssociation.


        :param last_modified_by: The last_modified_by of this MapAssociation.  # noqa: E501
        :type: str
        """

        self._last_modified_by = last_modified_by

    @property
    def last_modified_date(self):
        """Gets the last_modified_date of this MapAssociation.  # noqa: E501


        :return: The last_modified_date of this MapAssociation.  # noqa: E501
        :rtype: int
        """
        return self._last_modified_date

    @last_modified_date.setter
    def last_modified_date(self, last_modified_date):
        """Sets the last_modified_date of this MapAssociation.


        :param last_modified_date: The last_modified_date of this MapAssociation.  # noqa: E501
        :type: int
        """

        self._last_modified_date = last_modified_date

    @property
    def allowed_groups(self):
        """Gets the allowed_groups of this MapAssociation.  # noqa: E501


        :return: The allowed_groups of this MapAssociation.  # noqa: E501
        :rtype: list[str]
        """
        return self._allowed_groups

    @allowed_groups.setter
    def allowed_groups(self, allowed_groups):
        """Sets the allowed_groups of this MapAssociation.


        :param allowed_groups: The allowed_groups of this MapAssociation.  # noqa: E501
        :type: list[str]
        """

        self._allowed_groups = allowed_groups

    @property
    def organisation(self):
        """Gets the organisation of this MapAssociation.  # noqa: E501


        :return: The organisation of this MapAssociation.  # noqa: E501
        :rtype: str
        """
        return self._organisation

    @organisation.setter
    def organisation(self, organisation):
        """Sets the organisation of this MapAssociation.


        :param organisation: The organisation of this MapAssociation.  # noqa: E501
        :type: str
        """

        self._organisation = organisation

    @property
    def id(self):
        """Gets the id of this MapAssociation.  # noqa: E501


        :return: The id of this MapAssociation.  # noqa: E501
        :rtype: str
        """
        return self._id

    @id.setter
    def id(self, id):
        """Sets the id of this MapAssociation.


        :param id: The id of this MapAssociation.  # noqa: E501
        :type: str
        """

        self._id = id

    @property
    def source_id(self):
        """Gets the source_id of this MapAssociation.  # noqa: E501


        :return: The source_id of this MapAssociation.  # noqa: E501
        :rtype: str
        """
        return self._source_id

    @source_id.setter
    def source_id(self, source_id):
        """Sets the source_id of this MapAssociation.


        :param source_id: The source_id of this MapAssociation.  # noqa: E501
        :type: str
        """

        self._source_id = source_id

    @property
    def target_id(self):
        """Gets the target_id of this MapAssociation.  # noqa: E501


        :return: The target_id of this MapAssociation.  # noqa: E501
        :rtype: str
        """
        return self._target_id

    @target_id.setter
    def target_id(self, target_id):
        """Sets the target_id of this MapAssociation.


        :param target_id: The target_id of this MapAssociation.  # noqa: E501
        :type: str
        """

        self._target_id = target_id

    @property
    def type(self):
        """Gets the type of this MapAssociation.  # noqa: E501


        :return: The type of this MapAssociation.  # noqa: E501
        :rtype: str
        """
        return self._type

    @type.setter
    def type(self, type):
        """Sets the type of this MapAssociation.


        :param type: The type of this MapAssociation.  # noqa: E501
        :type: str
        """
        allowed_values = ["EXACT_MATCH", "LESS_SPECIFIC", "MORE_SPECIFIC", "NO_MATCH", "UNKNOWN"]  # noqa: E501
        if type not in allowed_values:
            raise ValueError(
                "Invalid value for `type` ({0}), must be one of {1}"  # noqa: E501
                .format(type, allowed_values)
            )

        self._type = type

    @property
    def excluded(self):
        """Gets the excluded of this MapAssociation.  # noqa: E501


        :return: The excluded of this MapAssociation.  # noqa: E501
        :rtype: bool
        """
        return self._excluded

    @excluded.setter
    def excluded(self, excluded):
        """Sets the excluded of this MapAssociation.


        :param excluded: The excluded of this MapAssociation.  # noqa: E501
        :type: bool
        """

        self._excluded = excluded

    @property
    def processed(self):
        """Gets the processed of this MapAssociation.  # noqa: E501


        :return: The processed of this MapAssociation.  # noqa: E501
        :rtype: bool
        """
        return self._processed

    @processed.setter
    def processed(self, processed):
        """Sets the processed of this MapAssociation.


        :param processed: The processed of this MapAssociation.  # noqa: E501
        :type: bool
        """

        self._processed = processed

    @property
    def localterm(self):
        """Gets the localterm of this MapAssociation.  # noqa: E501


        :return: The localterm of this MapAssociation.  # noqa: E501
        :rtype: str
        """
        return self._localterm

    @localterm.setter
    def localterm(self, localterm):
        """Sets the localterm of this MapAssociation.


        :param localterm: The localterm of this MapAssociation.  # noqa: E501
        :type: str
        """

        self._localterm = localterm

    @property
    def target_term(self):
        """Gets the target_term of this MapAssociation.  # noqa: E501


        :return: The target_term of this MapAssociation.  # noqa: E501
        :rtype: str
        """
        return self._target_term

    @target_term.setter
    def target_term(self, target_term):
        """Sets the target_term of this MapAssociation.


        :param target_term: The target_term of this MapAssociation.  # noqa: E501
        :type: str
        """

        self._target_term = target_term

    @property
    def status(self):
        """Gets the status of this MapAssociation.  # noqa: E501


        :return: The status of this MapAssociation.  # noqa: E501
        :rtype: str
        """
        return self._status

    @status.setter
    def status(self, status):
        """Sets the status of this MapAssociation.


        :param status: The status of this MapAssociation.  # noqa: E501
        :type: str
        """
        allowed_values = ["REVIEW", "FINALISED", "UNKNOWN"]  # noqa: E501
        if status not in allowed_values:
            raise ValueError(
                "Invalid value for `status` ({0}), must be one of {1}"  # noqa: E501
                .format(status, allowed_values)
            )

        self._status = status

    @property
    def code_list(self):
        """Gets the code_list of this MapAssociation.  # noqa: E501


        :return: The code_list of this MapAssociation.  # noqa: E501
        :rtype: str
        """
        return self._code_list

    @code_list.setter
    def code_list(self, code_list):
        """Sets the code_list of this MapAssociation.


        :param code_list: The code_list of this MapAssociation.  # noqa: E501
        :type: str
        """

        self._code_list = code_list

    @property
    def request(self):
        """Gets the request of this MapAssociation.  # noqa: E501


        :return: The request of this MapAssociation.  # noqa: E501
        :rtype: bool
        """
        return self._request

    @request.setter
    def request(self, request):
        """Sets the request of this MapAssociation.


        :param request: The request of this MapAssociation.  # noqa: E501
        :type: bool
        """

        self._request = request

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, MapAssociation):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        return not self == other
