# coding: utf-8

"""
    Pathnexus

    Pathnexus - a lab data harmonisation platform  # noqa: E501

    OpenAPI spec version: 4.6.0-SNAPSHOT
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""


import pprint
import re  # noqa: F401

import six

from pathnexus_client.com.termlex.pathnexus.model.code_type import CodeType  # noqa: F401,E501
from pathnexus_client.com.termlex.pathnexus.model.codeable_concept import CodeableConcept  # noqa: F401,E501
from pathnexus_client.com.termlex.pathnexus.model.coding import Coding  # noqa: F401,E501
from pathnexus_client.com.termlex.pathnexus.model.contact_point import ContactPoint  # noqa: F401,E501
from pathnexus_client.com.termlex.pathnexus.model.enumeration_endpoint_status import EnumerationEndpointStatus  # noqa: F401,E501
from pathnexus_client.com.termlex.pathnexus.model.extension import Extension  # noqa: F401,E501
from pathnexus_client.com.termlex.pathnexus.model.id_type import IdType  # noqa: F401,E501
from pathnexus_client.com.termlex.pathnexus.model.identifier import Identifier  # noqa: F401,E501
from pathnexus_client.com.termlex.pathnexus.model.meta import Meta  # noqa: F401,E501
from pathnexus_client.com.termlex.pathnexus.model.narrative import Narrative  # noqa: F401,E501
from pathnexus_client.com.termlex.pathnexus.model.organization import Organization  # noqa: F401,E501
from pathnexus_client.com.termlex.pathnexus.model.period import Period  # noqa: F401,E501
from pathnexus_client.com.termlex.pathnexus.model.reference import Reference  # noqa: F401,E501
from pathnexus_client.com.termlex.pathnexus.model.resource import Resource  # noqa: F401,E501
from pathnexus_client.com.termlex.pathnexus.model.string_type import StringType  # noqa: F401,E501
from pathnexus_client.com.termlex.pathnexus.model.uri_type import UriType  # noqa: F401,E501
from pathnexus_client.com.termlex.pathnexus.model.url_type import UrlType  # noqa: F401,E501


class Endpoint(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """

    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'format_comments_pre': 'list[str]',
        'format_comments_post': 'list[str]',
        'id': 'str',
        'meta': 'Meta',
        'implicit_rules': 'str',
        'language': 'str',
        'text': 'Narrative',
        'contained': 'list[Resource]',
        'extension': 'list[Extension]',
        'modifier_extension': 'list[Extension]',
        'identifier': 'list[Identifier]',
        'status': 'str',
        'connection_type': 'Coding',
        'name': 'str',
        'managing_organization': 'Reference',
        'managing_organization_target': 'Organization',
        'contact': 'list[ContactPoint]',
        'period': 'Period',
        'payload_type': 'list[CodeableConcept]',
        'payload_mime_type': 'list[CodeType]',
        'address': 'str',
        'header': 'list[StringType]',
        'payload_type_first_rep': 'CodeableConcept',
        'address_element': 'UrlType',
        'name_element': 'StringType',
        'status_element': 'EnumerationEndpointStatus',
        'contact_first_rep': 'ContactPoint',
        'identifier_first_rep': 'Identifier',
        'resource_type': 'str',
        'empty': 'bool',
        'id_element': 'IdType',
        'language_element': 'CodeType',
        'structure_fhir_version_enum': 'str',
        'implicit_rules_element': 'UriType',
        'id_base': 'str',
        'resource': 'bool',
        'primitive': 'bool',
        'boolean_primitive': 'bool',
        'date_time': 'bool'
    }

    attribute_map = {
        'format_comments_pre': 'formatCommentsPre',
        'format_comments_post': 'formatCommentsPost',
        'id': 'id',
        'meta': 'meta',
        'implicit_rules': 'implicitRules',
        'language': 'language',
        'text': 'text',
        'contained': 'contained',
        'extension': 'extension',
        'modifier_extension': 'modifierExtension',
        'identifier': 'identifier',
        'status': 'status',
        'connection_type': 'connectionType',
        'name': 'name',
        'managing_organization': 'managingOrganization',
        'managing_organization_target': 'managingOrganizationTarget',
        'contact': 'contact',
        'period': 'period',
        'payload_type': 'payloadType',
        'payload_mime_type': 'payloadMimeType',
        'address': 'address',
        'header': 'header',
        'payload_type_first_rep': 'payloadTypeFirstRep',
        'address_element': 'addressElement',
        'name_element': 'nameElement',
        'status_element': 'statusElement',
        'contact_first_rep': 'contactFirstRep',
        'identifier_first_rep': 'identifierFirstRep',
        'resource_type': 'resourceType',
        'empty': 'empty',
        'id_element': 'idElement',
        'language_element': 'languageElement',
        'structure_fhir_version_enum': 'structureFhirVersionEnum',
        'implicit_rules_element': 'implicitRulesElement',
        'id_base': 'idBase',
        'resource': 'resource',
        'primitive': 'primitive',
        'boolean_primitive': 'booleanPrimitive',
        'date_time': 'dateTime'
    }

    def __init__(self, format_comments_pre=None, format_comments_post=None, id=None, meta=None, implicit_rules=None, language=None, text=None, contained=None, extension=None, modifier_extension=None, identifier=None, status=None, connection_type=None, name=None, managing_organization=None, managing_organization_target=None, contact=None, period=None, payload_type=None, payload_mime_type=None, address=None, header=None, payload_type_first_rep=None, address_element=None, name_element=None, status_element=None, contact_first_rep=None, identifier_first_rep=None, resource_type=None, empty=None, id_element=None, language_element=None, structure_fhir_version_enum=None, implicit_rules_element=None, id_base=None, resource=None, primitive=None, boolean_primitive=None, date_time=None):  # noqa: E501
        """Endpoint - a model defined in Swagger"""  # noqa: E501

        self._format_comments_pre = None
        self._format_comments_post = None
        self._id = None
        self._meta = None
        self._implicit_rules = None
        self._language = None
        self._text = None
        self._contained = None
        self._extension = None
        self._modifier_extension = None
        self._identifier = None
        self._status = None
        self._connection_type = None
        self._name = None
        self._managing_organization = None
        self._managing_organization_target = None
        self._contact = None
        self._period = None
        self._payload_type = None
        self._payload_mime_type = None
        self._address = None
        self._header = None
        self._payload_type_first_rep = None
        self._address_element = None
        self._name_element = None
        self._status_element = None
        self._contact_first_rep = None
        self._identifier_first_rep = None
        self._resource_type = None
        self._empty = None
        self._id_element = None
        self._language_element = None
        self._structure_fhir_version_enum = None
        self._implicit_rules_element = None
        self._id_base = None
        self._resource = None
        self._primitive = None
        self._boolean_primitive = None
        self._date_time = None
        self.discriminator = None

        if format_comments_pre is not None:
            self.format_comments_pre = format_comments_pre
        if format_comments_post is not None:
            self.format_comments_post = format_comments_post
        if id is not None:
            self.id = id
        if meta is not None:
            self.meta = meta
        if implicit_rules is not None:
            self.implicit_rules = implicit_rules
        if language is not None:
            self.language = language
        if text is not None:
            self.text = text
        if contained is not None:
            self.contained = contained
        if extension is not None:
            self.extension = extension
        if modifier_extension is not None:
            self.modifier_extension = modifier_extension
        if identifier is not None:
            self.identifier = identifier
        if status is not None:
            self.status = status
        if connection_type is not None:
            self.connection_type = connection_type
        if name is not None:
            self.name = name
        if managing_organization is not None:
            self.managing_organization = managing_organization
        if managing_organization_target is not None:
            self.managing_organization_target = managing_organization_target
        if contact is not None:
            self.contact = contact
        if period is not None:
            self.period = period
        if payload_type is not None:
            self.payload_type = payload_type
        if payload_mime_type is not None:
            self.payload_mime_type = payload_mime_type
        if address is not None:
            self.address = address
        if header is not None:
            self.header = header
        if payload_type_first_rep is not None:
            self.payload_type_first_rep = payload_type_first_rep
        if address_element is not None:
            self.address_element = address_element
        if name_element is not None:
            self.name_element = name_element
        if status_element is not None:
            self.status_element = status_element
        if contact_first_rep is not None:
            self.contact_first_rep = contact_first_rep
        if identifier_first_rep is not None:
            self.identifier_first_rep = identifier_first_rep
        if resource_type is not None:
            self.resource_type = resource_type
        if empty is not None:
            self.empty = empty
        if id_element is not None:
            self.id_element = id_element
        if language_element is not None:
            self.language_element = language_element
        if structure_fhir_version_enum is not None:
            self.structure_fhir_version_enum = structure_fhir_version_enum
        if implicit_rules_element is not None:
            self.implicit_rules_element = implicit_rules_element
        if id_base is not None:
            self.id_base = id_base
        if resource is not None:
            self.resource = resource
        if primitive is not None:
            self.primitive = primitive
        if boolean_primitive is not None:
            self.boolean_primitive = boolean_primitive
        if date_time is not None:
            self.date_time = date_time

    @property
    def format_comments_pre(self):
        """Gets the format_comments_pre of this Endpoint.  # noqa: E501


        :return: The format_comments_pre of this Endpoint.  # noqa: E501
        :rtype: list[str]
        """
        return self._format_comments_pre

    @format_comments_pre.setter
    def format_comments_pre(self, format_comments_pre):
        """Sets the format_comments_pre of this Endpoint.


        :param format_comments_pre: The format_comments_pre of this Endpoint.  # noqa: E501
        :type: list[str]
        """

        self._format_comments_pre = format_comments_pre

    @property
    def format_comments_post(self):
        """Gets the format_comments_post of this Endpoint.  # noqa: E501


        :return: The format_comments_post of this Endpoint.  # noqa: E501
        :rtype: list[str]
        """
        return self._format_comments_post

    @format_comments_post.setter
    def format_comments_post(self, format_comments_post):
        """Sets the format_comments_post of this Endpoint.


        :param format_comments_post: The format_comments_post of this Endpoint.  # noqa: E501
        :type: list[str]
        """

        self._format_comments_post = format_comments_post

    @property
    def id(self):
        """Gets the id of this Endpoint.  # noqa: E501


        :return: The id of this Endpoint.  # noqa: E501
        :rtype: str
        """
        return self._id

    @id.setter
    def id(self, id):
        """Sets the id of this Endpoint.


        :param id: The id of this Endpoint.  # noqa: E501
        :type: str
        """

        self._id = id

    @property
    def meta(self):
        """Gets the meta of this Endpoint.  # noqa: E501


        :return: The meta of this Endpoint.  # noqa: E501
        :rtype: Meta
        """
        return self._meta

    @meta.setter
    def meta(self, meta):
        """Sets the meta of this Endpoint.


        :param meta: The meta of this Endpoint.  # noqa: E501
        :type: Meta
        """

        self._meta = meta

    @property
    def implicit_rules(self):
        """Gets the implicit_rules of this Endpoint.  # noqa: E501


        :return: The implicit_rules of this Endpoint.  # noqa: E501
        :rtype: str
        """
        return self._implicit_rules

    @implicit_rules.setter
    def implicit_rules(self, implicit_rules):
        """Sets the implicit_rules of this Endpoint.


        :param implicit_rules: The implicit_rules of this Endpoint.  # noqa: E501
        :type: str
        """

        self._implicit_rules = implicit_rules

    @property
    def language(self):
        """Gets the language of this Endpoint.  # noqa: E501


        :return: The language of this Endpoint.  # noqa: E501
        :rtype: str
        """
        return self._language

    @language.setter
    def language(self, language):
        """Sets the language of this Endpoint.


        :param language: The language of this Endpoint.  # noqa: E501
        :type: str
        """

        self._language = language

    @property
    def text(self):
        """Gets the text of this Endpoint.  # noqa: E501


        :return: The text of this Endpoint.  # noqa: E501
        :rtype: Narrative
        """
        return self._text

    @text.setter
    def text(self, text):
        """Sets the text of this Endpoint.


        :param text: The text of this Endpoint.  # noqa: E501
        :type: Narrative
        """

        self._text = text

    @property
    def contained(self):
        """Gets the contained of this Endpoint.  # noqa: E501


        :return: The contained of this Endpoint.  # noqa: E501
        :rtype: list[Resource]
        """
        return self._contained

    @contained.setter
    def contained(self, contained):
        """Sets the contained of this Endpoint.


        :param contained: The contained of this Endpoint.  # noqa: E501
        :type: list[Resource]
        """

        self._contained = contained

    @property
    def extension(self):
        """Gets the extension of this Endpoint.  # noqa: E501


        :return: The extension of this Endpoint.  # noqa: E501
        :rtype: list[Extension]
        """
        return self._extension

    @extension.setter
    def extension(self, extension):
        """Sets the extension of this Endpoint.


        :param extension: The extension of this Endpoint.  # noqa: E501
        :type: list[Extension]
        """

        self._extension = extension

    @property
    def modifier_extension(self):
        """Gets the modifier_extension of this Endpoint.  # noqa: E501


        :return: The modifier_extension of this Endpoint.  # noqa: E501
        :rtype: list[Extension]
        """
        return self._modifier_extension

    @modifier_extension.setter
    def modifier_extension(self, modifier_extension):
        """Sets the modifier_extension of this Endpoint.


        :param modifier_extension: The modifier_extension of this Endpoint.  # noqa: E501
        :type: list[Extension]
        """

        self._modifier_extension = modifier_extension

    @property
    def identifier(self):
        """Gets the identifier of this Endpoint.  # noqa: E501


        :return: The identifier of this Endpoint.  # noqa: E501
        :rtype: list[Identifier]
        """
        return self._identifier

    @identifier.setter
    def identifier(self, identifier):
        """Sets the identifier of this Endpoint.


        :param identifier: The identifier of this Endpoint.  # noqa: E501
        :type: list[Identifier]
        """

        self._identifier = identifier

    @property
    def status(self):
        """Gets the status of this Endpoint.  # noqa: E501


        :return: The status of this Endpoint.  # noqa: E501
        :rtype: str
        """
        return self._status

    @status.setter
    def status(self, status):
        """Sets the status of this Endpoint.


        :param status: The status of this Endpoint.  # noqa: E501
        :type: str
        """
        allowed_values = ["ACTIVE", "SUSPENDED", "ERROR", "OFF", "ENTEREDINERROR", "TEST", "NULL"]  # noqa: E501
        if status not in allowed_values:
            raise ValueError(
                "Invalid value for `status` ({0}), must be one of {1}"  # noqa: E501
                .format(status, allowed_values)
            )

        self._status = status

    @property
    def connection_type(self):
        """Gets the connection_type of this Endpoint.  # noqa: E501


        :return: The connection_type of this Endpoint.  # noqa: E501
        :rtype: Coding
        """
        return self._connection_type

    @connection_type.setter
    def connection_type(self, connection_type):
        """Sets the connection_type of this Endpoint.


        :param connection_type: The connection_type of this Endpoint.  # noqa: E501
        :type: Coding
        """

        self._connection_type = connection_type

    @property
    def name(self):
        """Gets the name of this Endpoint.  # noqa: E501


        :return: The name of this Endpoint.  # noqa: E501
        :rtype: str
        """
        return self._name

    @name.setter
    def name(self, name):
        """Sets the name of this Endpoint.


        :param name: The name of this Endpoint.  # noqa: E501
        :type: str
        """

        self._name = name

    @property
    def managing_organization(self):
        """Gets the managing_organization of this Endpoint.  # noqa: E501


        :return: The managing_organization of this Endpoint.  # noqa: E501
        :rtype: Reference
        """
        return self._managing_organization

    @managing_organization.setter
    def managing_organization(self, managing_organization):
        """Sets the managing_organization of this Endpoint.


        :param managing_organization: The managing_organization of this Endpoint.  # noqa: E501
        :type: Reference
        """

        self._managing_organization = managing_organization

    @property
    def managing_organization_target(self):
        """Gets the managing_organization_target of this Endpoint.  # noqa: E501


        :return: The managing_organization_target of this Endpoint.  # noqa: E501
        :rtype: Organization
        """
        return self._managing_organization_target

    @managing_organization_target.setter
    def managing_organization_target(self, managing_organization_target):
        """Sets the managing_organization_target of this Endpoint.


        :param managing_organization_target: The managing_organization_target of this Endpoint.  # noqa: E501
        :type: Organization
        """

        self._managing_organization_target = managing_organization_target

    @property
    def contact(self):
        """Gets the contact of this Endpoint.  # noqa: E501


        :return: The contact of this Endpoint.  # noqa: E501
        :rtype: list[ContactPoint]
        """
        return self._contact

    @contact.setter
    def contact(self, contact):
        """Sets the contact of this Endpoint.


        :param contact: The contact of this Endpoint.  # noqa: E501
        :type: list[ContactPoint]
        """

        self._contact = contact

    @property
    def period(self):
        """Gets the period of this Endpoint.  # noqa: E501


        :return: The period of this Endpoint.  # noqa: E501
        :rtype: Period
        """
        return self._period

    @period.setter
    def period(self, period):
        """Sets the period of this Endpoint.


        :param period: The period of this Endpoint.  # noqa: E501
        :type: Period
        """

        self._period = period

    @property
    def payload_type(self):
        """Gets the payload_type of this Endpoint.  # noqa: E501


        :return: The payload_type of this Endpoint.  # noqa: E501
        :rtype: list[CodeableConcept]
        """
        return self._payload_type

    @payload_type.setter
    def payload_type(self, payload_type):
        """Sets the payload_type of this Endpoint.


        :param payload_type: The payload_type of this Endpoint.  # noqa: E501
        :type: list[CodeableConcept]
        """

        self._payload_type = payload_type

    @property
    def payload_mime_type(self):
        """Gets the payload_mime_type of this Endpoint.  # noqa: E501


        :return: The payload_mime_type of this Endpoint.  # noqa: E501
        :rtype: list[CodeType]
        """
        return self._payload_mime_type

    @payload_mime_type.setter
    def payload_mime_type(self, payload_mime_type):
        """Sets the payload_mime_type of this Endpoint.


        :param payload_mime_type: The payload_mime_type of this Endpoint.  # noqa: E501
        :type: list[CodeType]
        """

        self._payload_mime_type = payload_mime_type

    @property
    def address(self):
        """Gets the address of this Endpoint.  # noqa: E501


        :return: The address of this Endpoint.  # noqa: E501
        :rtype: str
        """
        return self._address

    @address.setter
    def address(self, address):
        """Sets the address of this Endpoint.


        :param address: The address of this Endpoint.  # noqa: E501
        :type: str
        """

        self._address = address

    @property
    def header(self):
        """Gets the header of this Endpoint.  # noqa: E501


        :return: The header of this Endpoint.  # noqa: E501
        :rtype: list[StringType]
        """
        return self._header

    @header.setter
    def header(self, header):
        """Sets the header of this Endpoint.


        :param header: The header of this Endpoint.  # noqa: E501
        :type: list[StringType]
        """

        self._header = header

    @property
    def payload_type_first_rep(self):
        """Gets the payload_type_first_rep of this Endpoint.  # noqa: E501


        :return: The payload_type_first_rep of this Endpoint.  # noqa: E501
        :rtype: CodeableConcept
        """
        return self._payload_type_first_rep

    @payload_type_first_rep.setter
    def payload_type_first_rep(self, payload_type_first_rep):
        """Sets the payload_type_first_rep of this Endpoint.


        :param payload_type_first_rep: The payload_type_first_rep of this Endpoint.  # noqa: E501
        :type: CodeableConcept
        """

        self._payload_type_first_rep = payload_type_first_rep

    @property
    def address_element(self):
        """Gets the address_element of this Endpoint.  # noqa: E501


        :return: The address_element of this Endpoint.  # noqa: E501
        :rtype: UrlType
        """
        return self._address_element

    @address_element.setter
    def address_element(self, address_element):
        """Sets the address_element of this Endpoint.


        :param address_element: The address_element of this Endpoint.  # noqa: E501
        :type: UrlType
        """

        self._address_element = address_element

    @property
    def name_element(self):
        """Gets the name_element of this Endpoint.  # noqa: E501


        :return: The name_element of this Endpoint.  # noqa: E501
        :rtype: StringType
        """
        return self._name_element

    @name_element.setter
    def name_element(self, name_element):
        """Sets the name_element of this Endpoint.


        :param name_element: The name_element of this Endpoint.  # noqa: E501
        :type: StringType
        """

        self._name_element = name_element

    @property
    def status_element(self):
        """Gets the status_element of this Endpoint.  # noqa: E501


        :return: The status_element of this Endpoint.  # noqa: E501
        :rtype: EnumerationEndpointStatus
        """
        return self._status_element

    @status_element.setter
    def status_element(self, status_element):
        """Sets the status_element of this Endpoint.


        :param status_element: The status_element of this Endpoint.  # noqa: E501
        :type: EnumerationEndpointStatus
        """

        self._status_element = status_element

    @property
    def contact_first_rep(self):
        """Gets the contact_first_rep of this Endpoint.  # noqa: E501


        :return: The contact_first_rep of this Endpoint.  # noqa: E501
        :rtype: ContactPoint
        """
        return self._contact_first_rep

    @contact_first_rep.setter
    def contact_first_rep(self, contact_first_rep):
        """Sets the contact_first_rep of this Endpoint.


        :param contact_first_rep: The contact_first_rep of this Endpoint.  # noqa: E501
        :type: ContactPoint
        """

        self._contact_first_rep = contact_first_rep

    @property
    def identifier_first_rep(self):
        """Gets the identifier_first_rep of this Endpoint.  # noqa: E501


        :return: The identifier_first_rep of this Endpoint.  # noqa: E501
        :rtype: Identifier
        """
        return self._identifier_first_rep

    @identifier_first_rep.setter
    def identifier_first_rep(self, identifier_first_rep):
        """Sets the identifier_first_rep of this Endpoint.


        :param identifier_first_rep: The identifier_first_rep of this Endpoint.  # noqa: E501
        :type: Identifier
        """

        self._identifier_first_rep = identifier_first_rep

    @property
    def resource_type(self):
        """Gets the resource_type of this Endpoint.  # noqa: E501


        :return: The resource_type of this Endpoint.  # noqa: E501
        :rtype: str
        """
        return self._resource_type

    @resource_type.setter
    def resource_type(self, resource_type):
        """Sets the resource_type of this Endpoint.


        :param resource_type: The resource_type of this Endpoint.  # noqa: E501
        :type: str
        """
        allowed_values = ["Account", "ActivityDefinition", "AdverseEvent", "AllergyIntolerance", "Appointment", "AppointmentResponse", "AuditEvent", "Basic", "Binary", "BiologicallyDerivedProduct", "BodyStructure", "Bundle", "CapabilityStatement", "CarePlan", "CareTeam", "CatalogEntry", "ChargeItem", "ChargeItemDefinition", "Claim", "ClaimResponse", "ClinicalImpression", "CodeSystem", "Communication", "CommunicationRequest", "CompartmentDefinition", "Composition", "ConceptMap", "Condition", "Consent", "Contract", "Coverage", "CoverageEligibilityRequest", "CoverageEligibilityResponse", "DetectedIssue", "Device", "DeviceDefinition", "DeviceMetric", "DeviceRequest", "DeviceUseStatement", "DiagnosticReport", "DocumentManifest", "DocumentReference", "EffectEvidenceSynthesis", "Encounter", "Endpoint", "EnrollmentRequest", "EnrollmentResponse", "EpisodeOfCare", "EventDefinition", "Evidence", "EvidenceVariable", "ExampleScenario", "ExplanationOfBenefit", "FamilyMemberHistory", "Flag", "Goal", "GraphDefinition", "Group", "GuidanceResponse", "HealthcareService", "ImagingStudy", "Immunization", "ImmunizationEvaluation", "ImmunizationRecommendation", "ImplementationGuide", "InsurancePlan", "Invoice", "Library", "Linkage", "List", "Location", "Measure", "MeasureReport", "Media", "Medication", "MedicationAdministration", "MedicationDispense", "MedicationKnowledge", "MedicationRequest", "MedicationStatement", "MedicinalProduct", "MedicinalProductAuthorization", "MedicinalProductContraindication", "MedicinalProductIndication", "MedicinalProductIngredient", "MedicinalProductInteraction", "MedicinalProductManufactured", "MedicinalProductPackaged", "MedicinalProductPharmaceutical", "MedicinalProductUndesirableEffect", "MessageDefinition", "MessageHeader", "MolecularSequence", "NamingSystem", "NutritionOrder", "Observation", "ObservationDefinition", "OperationDefinition", "OperationOutcome", "Organization", "OrganizationAffiliation", "Parameters", "Patient", "PaymentNotice", "PaymentReconciliation", "Person", "PlanDefinition", "Practitioner", "PractitionerRole", "Procedure", "Provenance", "Questionnaire", "QuestionnaireResponse", "RelatedPerson", "RequestGroup", "ResearchDefinition", "ResearchElementDefinition", "ResearchStudy", "ResearchSubject", "RiskAssessment", "RiskEvidenceSynthesis", "Schedule", "SearchParameter", "ServiceRequest", "Slot", "Specimen", "SpecimenDefinition", "StructureDefinition", "StructureMap", "Subscription", "Substance", "SubstanceNucleicAcid", "SubstancePolymer", "SubstanceProtein", "SubstanceReferenceInformation", "SubstanceSourceMaterial", "SubstanceSpecification", "SupplyDelivery", "SupplyRequest", "Task", "TerminologyCapabilities", "TestReport", "TestScript", "ValueSet", "VerificationResult", "VisionPrescription"]  # noqa: E501
        if resource_type not in allowed_values:
            raise ValueError(
                "Invalid value for `resource_type` ({0}), must be one of {1}"  # noqa: E501
                .format(resource_type, allowed_values)
            )

        self._resource_type = resource_type

    @property
    def empty(self):
        """Gets the empty of this Endpoint.  # noqa: E501


        :return: The empty of this Endpoint.  # noqa: E501
        :rtype: bool
        """
        return self._empty

    @empty.setter
    def empty(self, empty):
        """Sets the empty of this Endpoint.


        :param empty: The empty of this Endpoint.  # noqa: E501
        :type: bool
        """

        self._empty = empty

    @property
    def id_element(self):
        """Gets the id_element of this Endpoint.  # noqa: E501


        :return: The id_element of this Endpoint.  # noqa: E501
        :rtype: IdType
        """
        return self._id_element

    @id_element.setter
    def id_element(self, id_element):
        """Sets the id_element of this Endpoint.


        :param id_element: The id_element of this Endpoint.  # noqa: E501
        :type: IdType
        """

        self._id_element = id_element

    @property
    def language_element(self):
        """Gets the language_element of this Endpoint.  # noqa: E501


        :return: The language_element of this Endpoint.  # noqa: E501
        :rtype: CodeType
        """
        return self._language_element

    @language_element.setter
    def language_element(self, language_element):
        """Sets the language_element of this Endpoint.


        :param language_element: The language_element of this Endpoint.  # noqa: E501
        :type: CodeType
        """

        self._language_element = language_element

    @property
    def structure_fhir_version_enum(self):
        """Gets the structure_fhir_version_enum of this Endpoint.  # noqa: E501


        :return: The structure_fhir_version_enum of this Endpoint.  # noqa: E501
        :rtype: str
        """
        return self._structure_fhir_version_enum

    @structure_fhir_version_enum.setter
    def structure_fhir_version_enum(self, structure_fhir_version_enum):
        """Sets the structure_fhir_version_enum of this Endpoint.


        :param structure_fhir_version_enum: The structure_fhir_version_enum of this Endpoint.  # noqa: E501
        :type: str
        """
        allowed_values = ["DSTU2", "DSTU2_HL7ORG", "DSTU2_1", "DSTU3", "R4", "R5"]  # noqa: E501
        if structure_fhir_version_enum not in allowed_values:
            raise ValueError(
                "Invalid value for `structure_fhir_version_enum` ({0}), must be one of {1}"  # noqa: E501
                .format(structure_fhir_version_enum, allowed_values)
            )

        self._structure_fhir_version_enum = structure_fhir_version_enum

    @property
    def implicit_rules_element(self):
        """Gets the implicit_rules_element of this Endpoint.  # noqa: E501


        :return: The implicit_rules_element of this Endpoint.  # noqa: E501
        :rtype: UriType
        """
        return self._implicit_rules_element

    @implicit_rules_element.setter
    def implicit_rules_element(self, implicit_rules_element):
        """Sets the implicit_rules_element of this Endpoint.


        :param implicit_rules_element: The implicit_rules_element of this Endpoint.  # noqa: E501
        :type: UriType
        """

        self._implicit_rules_element = implicit_rules_element

    @property
    def id_base(self):
        """Gets the id_base of this Endpoint.  # noqa: E501


        :return: The id_base of this Endpoint.  # noqa: E501
        :rtype: str
        """
        return self._id_base

    @id_base.setter
    def id_base(self, id_base):
        """Sets the id_base of this Endpoint.


        :param id_base: The id_base of this Endpoint.  # noqa: E501
        :type: str
        """

        self._id_base = id_base

    @property
    def resource(self):
        """Gets the resource of this Endpoint.  # noqa: E501


        :return: The resource of this Endpoint.  # noqa: E501
        :rtype: bool
        """
        return self._resource

    @resource.setter
    def resource(self, resource):
        """Sets the resource of this Endpoint.


        :param resource: The resource of this Endpoint.  # noqa: E501
        :type: bool
        """

        self._resource = resource

    @property
    def primitive(self):
        """Gets the primitive of this Endpoint.  # noqa: E501


        :return: The primitive of this Endpoint.  # noqa: E501
        :rtype: bool
        """
        return self._primitive

    @primitive.setter
    def primitive(self, primitive):
        """Sets the primitive of this Endpoint.


        :param primitive: The primitive of this Endpoint.  # noqa: E501
        :type: bool
        """

        self._primitive = primitive

    @property
    def boolean_primitive(self):
        """Gets the boolean_primitive of this Endpoint.  # noqa: E501


        :return: The boolean_primitive of this Endpoint.  # noqa: E501
        :rtype: bool
        """
        return self._boolean_primitive

    @boolean_primitive.setter
    def boolean_primitive(self, boolean_primitive):
        """Sets the boolean_primitive of this Endpoint.


        :param boolean_primitive: The boolean_primitive of this Endpoint.  # noqa: E501
        :type: bool
        """

        self._boolean_primitive = boolean_primitive

    @property
    def date_time(self):
        """Gets the date_time of this Endpoint.  # noqa: E501


        :return: The date_time of this Endpoint.  # noqa: E501
        :rtype: bool
        """
        return self._date_time

    @date_time.setter
    def date_time(self, date_time):
        """Sets the date_time of this Endpoint.


        :param date_time: The date_time of this Endpoint.  # noqa: E501
        :type: bool
        """

        self._date_time = date_time

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, Endpoint):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        return not self == other
