# coding: utf-8

"""
    Pathnexus

    Pathnexus - a lab data harmonisation platform  # noqa: E501

    OpenAPI spec version: 5.8.0
    Contact: support@termlex.com
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""


import pprint
import re  # noqa: F401

import six

from pathnexus.com.termlex.pathnexus.model.codeable_concept import CodeableConcept  # noqa: F401,E501
from pathnexus.com.termlex.pathnexus.model.enumeration_identifier_use import EnumerationIdentifierUse  # noqa: F401,E501
from pathnexus.com.termlex.pathnexus.model.extension import Extension  # noqa: F401,E501
from pathnexus.com.termlex.pathnexus.model.organization import Organization  # noqa: F401,E501
from pathnexus.com.termlex.pathnexus.model.period import Period  # noqa: F401,E501
from pathnexus.com.termlex.pathnexus.model.reference import Reference  # noqa: F401,E501
from pathnexus.com.termlex.pathnexus.model.string_type import StringType  # noqa: F401,E501
from pathnexus.com.termlex.pathnexus.model.uri_type import UriType  # noqa: F401,E501


class Identifier(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """

    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'format_comments_pre': 'list[str]',
        'format_comments_post': 'list[str]',
        'id': 'str',
        'extension': 'list[Extension]',
        'disallow_extensions': 'bool',
        'use': 'str',
        'type': 'CodeableConcept',
        'system': 'str',
        'value': 'str',
        'period': 'Period',
        'assigner': 'Reference',
        'assigner_target': 'Organization',
        'use_element': 'EnumerationIdentifierUse',
        'value_element': 'StringType',
        'system_element': 'UriType',
        'empty': 'bool',
        'id_element': 'StringType',
        'extension_first_rep': 'Extension',
        'id_base': 'str',
        'resource': 'bool',
        'boolean_primitive': 'bool',
        'date_time': 'bool',
        'primitive': 'bool'
    }

    attribute_map = {
        'format_comments_pre': 'formatCommentsPre',
        'format_comments_post': 'formatCommentsPost',
        'id': 'id',
        'extension': 'extension',
        'disallow_extensions': 'disallowExtensions',
        'use': 'use',
        'type': 'type',
        'system': 'system',
        'value': 'value',
        'period': 'period',
        'assigner': 'assigner',
        'assigner_target': 'assignerTarget',
        'use_element': 'useElement',
        'value_element': 'valueElement',
        'system_element': 'systemElement',
        'empty': 'empty',
        'id_element': 'idElement',
        'extension_first_rep': 'extensionFirstRep',
        'id_base': 'idBase',
        'resource': 'resource',
        'boolean_primitive': 'booleanPrimitive',
        'date_time': 'dateTime',
        'primitive': 'primitive'
    }

    def __init__(self, format_comments_pre=None, format_comments_post=None, id=None, extension=None, disallow_extensions=None, use=None, type=None, system=None, value=None, period=None, assigner=None, assigner_target=None, use_element=None, value_element=None, system_element=None, empty=None, id_element=None, extension_first_rep=None, id_base=None, resource=None, boolean_primitive=None, date_time=None, primitive=None):  # noqa: E501
        """Identifier - a model defined in Swagger"""  # noqa: E501

        self._format_comments_pre = None
        self._format_comments_post = None
        self._id = None
        self._extension = None
        self._disallow_extensions = None
        self._use = None
        self._type = None
        self._system = None
        self._value = None
        self._period = None
        self._assigner = None
        self._assigner_target = None
        self._use_element = None
        self._value_element = None
        self._system_element = None
        self._empty = None
        self._id_element = None
        self._extension_first_rep = None
        self._id_base = None
        self._resource = None
        self._boolean_primitive = None
        self._date_time = None
        self._primitive = None
        self.discriminator = None

        if format_comments_pre is not None:
            self.format_comments_pre = format_comments_pre
        if format_comments_post is not None:
            self.format_comments_post = format_comments_post
        if id is not None:
            self.id = id
        if extension is not None:
            self.extension = extension
        if disallow_extensions is not None:
            self.disallow_extensions = disallow_extensions
        if use is not None:
            self.use = use
        if type is not None:
            self.type = type
        if system is not None:
            self.system = system
        if value is not None:
            self.value = value
        if period is not None:
            self.period = period
        if assigner is not None:
            self.assigner = assigner
        if assigner_target is not None:
            self.assigner_target = assigner_target
        if use_element is not None:
            self.use_element = use_element
        if value_element is not None:
            self.value_element = value_element
        if system_element is not None:
            self.system_element = system_element
        if empty is not None:
            self.empty = empty
        if id_element is not None:
            self.id_element = id_element
        if extension_first_rep is not None:
            self.extension_first_rep = extension_first_rep
        if id_base is not None:
            self.id_base = id_base
        if resource is not None:
            self.resource = resource
        if boolean_primitive is not None:
            self.boolean_primitive = boolean_primitive
        if date_time is not None:
            self.date_time = date_time
        if primitive is not None:
            self.primitive = primitive

    @property
    def format_comments_pre(self):
        """Gets the format_comments_pre of this Identifier.  # noqa: E501


        :return: The format_comments_pre of this Identifier.  # noqa: E501
        :rtype: list[str]
        """
        return self._format_comments_pre

    @format_comments_pre.setter
    def format_comments_pre(self, format_comments_pre):
        """Sets the format_comments_pre of this Identifier.


        :param format_comments_pre: The format_comments_pre of this Identifier.  # noqa: E501
        :type: list[str]
        """

        self._format_comments_pre = format_comments_pre

    @property
    def format_comments_post(self):
        """Gets the format_comments_post of this Identifier.  # noqa: E501


        :return: The format_comments_post of this Identifier.  # noqa: E501
        :rtype: list[str]
        """
        return self._format_comments_post

    @format_comments_post.setter
    def format_comments_post(self, format_comments_post):
        """Sets the format_comments_post of this Identifier.


        :param format_comments_post: The format_comments_post of this Identifier.  # noqa: E501
        :type: list[str]
        """

        self._format_comments_post = format_comments_post

    @property
    def id(self):
        """Gets the id of this Identifier.  # noqa: E501


        :return: The id of this Identifier.  # noqa: E501
        :rtype: str
        """
        return self._id

    @id.setter
    def id(self, id):
        """Sets the id of this Identifier.


        :param id: The id of this Identifier.  # noqa: E501
        :type: str
        """

        self._id = id

    @property
    def extension(self):
        """Gets the extension of this Identifier.  # noqa: E501


        :return: The extension of this Identifier.  # noqa: E501
        :rtype: list[Extension]
        """
        return self._extension

    @extension.setter
    def extension(self, extension):
        """Sets the extension of this Identifier.


        :param extension: The extension of this Identifier.  # noqa: E501
        :type: list[Extension]
        """

        self._extension = extension

    @property
    def disallow_extensions(self):
        """Gets the disallow_extensions of this Identifier.  # noqa: E501


        :return: The disallow_extensions of this Identifier.  # noqa: E501
        :rtype: bool
        """
        return self._disallow_extensions

    @disallow_extensions.setter
    def disallow_extensions(self, disallow_extensions):
        """Sets the disallow_extensions of this Identifier.


        :param disallow_extensions: The disallow_extensions of this Identifier.  # noqa: E501
        :type: bool
        """

        self._disallow_extensions = disallow_extensions

    @property
    def use(self):
        """Gets the use of this Identifier.  # noqa: E501


        :return: The use of this Identifier.  # noqa: E501
        :rtype: str
        """
        return self._use

    @use.setter
    def use(self, use):
        """Sets the use of this Identifier.


        :param use: The use of this Identifier.  # noqa: E501
        :type: str
        """
        allowed_values = ["USUAL", "OFFICIAL", "TEMP", "SECONDARY", "OLD", "NULL"]  # noqa: E501
        if use not in allowed_values:
            raise ValueError(
                "Invalid value for `use` ({0}), must be one of {1}"  # noqa: E501
                .format(use, allowed_values)
            )

        self._use = use

    @property
    def type(self):
        """Gets the type of this Identifier.  # noqa: E501


        :return: The type of this Identifier.  # noqa: E501
        :rtype: CodeableConcept
        """
        return self._type

    @type.setter
    def type(self, type):
        """Sets the type of this Identifier.


        :param type: The type of this Identifier.  # noqa: E501
        :type: CodeableConcept
        """

        self._type = type

    @property
    def system(self):
        """Gets the system of this Identifier.  # noqa: E501


        :return: The system of this Identifier.  # noqa: E501
        :rtype: str
        """
        return self._system

    @system.setter
    def system(self, system):
        """Sets the system of this Identifier.


        :param system: The system of this Identifier.  # noqa: E501
        :type: str
        """

        self._system = system

    @property
    def value(self):
        """Gets the value of this Identifier.  # noqa: E501


        :return: The value of this Identifier.  # noqa: E501
        :rtype: str
        """
        return self._value

    @value.setter
    def value(self, value):
        """Sets the value of this Identifier.


        :param value: The value of this Identifier.  # noqa: E501
        :type: str
        """

        self._value = value

    @property
    def period(self):
        """Gets the period of this Identifier.  # noqa: E501


        :return: The period of this Identifier.  # noqa: E501
        :rtype: Period
        """
        return self._period

    @period.setter
    def period(self, period):
        """Sets the period of this Identifier.


        :param period: The period of this Identifier.  # noqa: E501
        :type: Period
        """

        self._period = period

    @property
    def assigner(self):
        """Gets the assigner of this Identifier.  # noqa: E501


        :return: The assigner of this Identifier.  # noqa: E501
        :rtype: Reference
        """
        return self._assigner

    @assigner.setter
    def assigner(self, assigner):
        """Sets the assigner of this Identifier.


        :param assigner: The assigner of this Identifier.  # noqa: E501
        :type: Reference
        """

        self._assigner = assigner

    @property
    def assigner_target(self):
        """Gets the assigner_target of this Identifier.  # noqa: E501


        :return: The assigner_target of this Identifier.  # noqa: E501
        :rtype: Organization
        """
        return self._assigner_target

    @assigner_target.setter
    def assigner_target(self, assigner_target):
        """Sets the assigner_target of this Identifier.


        :param assigner_target: The assigner_target of this Identifier.  # noqa: E501
        :type: Organization
        """

        self._assigner_target = assigner_target

    @property
    def use_element(self):
        """Gets the use_element of this Identifier.  # noqa: E501


        :return: The use_element of this Identifier.  # noqa: E501
        :rtype: EnumerationIdentifierUse
        """
        return self._use_element

    @use_element.setter
    def use_element(self, use_element):
        """Sets the use_element of this Identifier.


        :param use_element: The use_element of this Identifier.  # noqa: E501
        :type: EnumerationIdentifierUse
        """

        self._use_element = use_element

    @property
    def value_element(self):
        """Gets the value_element of this Identifier.  # noqa: E501


        :return: The value_element of this Identifier.  # noqa: E501
        :rtype: StringType
        """
        return self._value_element

    @value_element.setter
    def value_element(self, value_element):
        """Sets the value_element of this Identifier.


        :param value_element: The value_element of this Identifier.  # noqa: E501
        :type: StringType
        """

        self._value_element = value_element

    @property
    def system_element(self):
        """Gets the system_element of this Identifier.  # noqa: E501


        :return: The system_element of this Identifier.  # noqa: E501
        :rtype: UriType
        """
        return self._system_element

    @system_element.setter
    def system_element(self, system_element):
        """Sets the system_element of this Identifier.


        :param system_element: The system_element of this Identifier.  # noqa: E501
        :type: UriType
        """

        self._system_element = system_element

    @property
    def empty(self):
        """Gets the empty of this Identifier.  # noqa: E501


        :return: The empty of this Identifier.  # noqa: E501
        :rtype: bool
        """
        return self._empty

    @empty.setter
    def empty(self, empty):
        """Sets the empty of this Identifier.


        :param empty: The empty of this Identifier.  # noqa: E501
        :type: bool
        """

        self._empty = empty

    @property
    def id_element(self):
        """Gets the id_element of this Identifier.  # noqa: E501


        :return: The id_element of this Identifier.  # noqa: E501
        :rtype: StringType
        """
        return self._id_element

    @id_element.setter
    def id_element(self, id_element):
        """Sets the id_element of this Identifier.


        :param id_element: The id_element of this Identifier.  # noqa: E501
        :type: StringType
        """

        self._id_element = id_element

    @property
    def extension_first_rep(self):
        """Gets the extension_first_rep of this Identifier.  # noqa: E501


        :return: The extension_first_rep of this Identifier.  # noqa: E501
        :rtype: Extension
        """
        return self._extension_first_rep

    @extension_first_rep.setter
    def extension_first_rep(self, extension_first_rep):
        """Sets the extension_first_rep of this Identifier.


        :param extension_first_rep: The extension_first_rep of this Identifier.  # noqa: E501
        :type: Extension
        """

        self._extension_first_rep = extension_first_rep

    @property
    def id_base(self):
        """Gets the id_base of this Identifier.  # noqa: E501


        :return: The id_base of this Identifier.  # noqa: E501
        :rtype: str
        """
        return self._id_base

    @id_base.setter
    def id_base(self, id_base):
        """Sets the id_base of this Identifier.


        :param id_base: The id_base of this Identifier.  # noqa: E501
        :type: str
        """

        self._id_base = id_base

    @property
    def resource(self):
        """Gets the resource of this Identifier.  # noqa: E501


        :return: The resource of this Identifier.  # noqa: E501
        :rtype: bool
        """
        return self._resource

    @resource.setter
    def resource(self, resource):
        """Sets the resource of this Identifier.


        :param resource: The resource of this Identifier.  # noqa: E501
        :type: bool
        """

        self._resource = resource

    @property
    def boolean_primitive(self):
        """Gets the boolean_primitive of this Identifier.  # noqa: E501


        :return: The boolean_primitive of this Identifier.  # noqa: E501
        :rtype: bool
        """
        return self._boolean_primitive

    @boolean_primitive.setter
    def boolean_primitive(self, boolean_primitive):
        """Sets the boolean_primitive of this Identifier.


        :param boolean_primitive: The boolean_primitive of this Identifier.  # noqa: E501
        :type: bool
        """

        self._boolean_primitive = boolean_primitive

    @property
    def date_time(self):
        """Gets the date_time of this Identifier.  # noqa: E501


        :return: The date_time of this Identifier.  # noqa: E501
        :rtype: bool
        """
        return self._date_time

    @date_time.setter
    def date_time(self, date_time):
        """Sets the date_time of this Identifier.


        :param date_time: The date_time of this Identifier.  # noqa: E501
        :type: bool
        """

        self._date_time = date_time

    @property
    def primitive(self):
        """Gets the primitive of this Identifier.  # noqa: E501


        :return: The primitive of this Identifier.  # noqa: E501
        :rtype: bool
        """
        return self._primitive

    @primitive.setter
    def primitive(self, primitive):
        """Sets the primitive of this Identifier.


        :param primitive: The primitive of this Identifier.  # noqa: E501
        :type: bool
        """

        self._primitive = primitive

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, Identifier):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        return not self == other
