# coding: utf-8

"""
    Pathnexus

    Pathnexus - a lab data harmonisation platform  # noqa: E501

    OpenAPI spec version: 5.8.0
    Contact: support@termlex.com
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""


import pprint
import re  # noqa: F401

import six

from pathnexus.com.termlex.pathnexus.model.enum_factory_concept_map_group_unmapped_mode import EnumFactoryConceptMapGroupUnmappedMode  # noqa: F401,E501
from pathnexus.com.termlex.pathnexus.model.extension import Extension  # noqa: F401,E501
from pathnexus.com.termlex.pathnexus.model.string_type import StringType  # noqa: F401,E501


class EnumerationConceptMapGroupUnmappedMode(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """

    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'format_comments_pre': 'list[str]',
        'format_comments_post': 'list[str]',
        'id': 'str',
        'extension': 'list[Extension]',
        'disallow_extensions': 'bool',
        'version': 'str',
        'code': 'str',
        'system': 'str',
        'enum_factory': 'EnumFactoryConceptMapGroupUnmappedMode',
        'display': 'str',
        'value_as_string': 'str',
        'value': 'str',
        'empty': 'bool',
        'primitive': 'bool',
        'id_element': 'StringType',
        'extension_first_rep': 'Extension',
        'id_base': 'str',
        'resource': 'bool',
        'boolean_primitive': 'bool',
        'date_time': 'bool'
    }

    attribute_map = {
        'format_comments_pre': 'formatCommentsPre',
        'format_comments_post': 'formatCommentsPost',
        'id': 'id',
        'extension': 'extension',
        'disallow_extensions': 'disallowExtensions',
        'version': 'version',
        'code': 'code',
        'system': 'system',
        'enum_factory': 'enumFactory',
        'display': 'display',
        'value_as_string': 'valueAsString',
        'value': 'value',
        'empty': 'empty',
        'primitive': 'primitive',
        'id_element': 'idElement',
        'extension_first_rep': 'extensionFirstRep',
        'id_base': 'idBase',
        'resource': 'resource',
        'boolean_primitive': 'booleanPrimitive',
        'date_time': 'dateTime'
    }

    def __init__(self, format_comments_pre=None, format_comments_post=None, id=None, extension=None, disallow_extensions=None, version=None, code=None, system=None, enum_factory=None, display=None, value_as_string=None, value=None, empty=None, primitive=None, id_element=None, extension_first_rep=None, id_base=None, resource=None, boolean_primitive=None, date_time=None):  # noqa: E501
        """EnumerationConceptMapGroupUnmappedMode - a model defined in Swagger"""  # noqa: E501

        self._format_comments_pre = None
        self._format_comments_post = None
        self._id = None
        self._extension = None
        self._disallow_extensions = None
        self._version = None
        self._code = None
        self._system = None
        self._enum_factory = None
        self._display = None
        self._value_as_string = None
        self._value = None
        self._empty = None
        self._primitive = None
        self._id_element = None
        self._extension_first_rep = None
        self._id_base = None
        self._resource = None
        self._boolean_primitive = None
        self._date_time = None
        self.discriminator = None

        if format_comments_pre is not None:
            self.format_comments_pre = format_comments_pre
        if format_comments_post is not None:
            self.format_comments_post = format_comments_post
        if id is not None:
            self.id = id
        if extension is not None:
            self.extension = extension
        if disallow_extensions is not None:
            self.disallow_extensions = disallow_extensions
        if version is not None:
            self.version = version
        if code is not None:
            self.code = code
        if system is not None:
            self.system = system
        if enum_factory is not None:
            self.enum_factory = enum_factory
        if display is not None:
            self.display = display
        if value_as_string is not None:
            self.value_as_string = value_as_string
        if value is not None:
            self.value = value
        if empty is not None:
            self.empty = empty
        if primitive is not None:
            self.primitive = primitive
        if id_element is not None:
            self.id_element = id_element
        if extension_first_rep is not None:
            self.extension_first_rep = extension_first_rep
        if id_base is not None:
            self.id_base = id_base
        if resource is not None:
            self.resource = resource
        if boolean_primitive is not None:
            self.boolean_primitive = boolean_primitive
        if date_time is not None:
            self.date_time = date_time

    @property
    def format_comments_pre(self):
        """Gets the format_comments_pre of this EnumerationConceptMapGroupUnmappedMode.  # noqa: E501


        :return: The format_comments_pre of this EnumerationConceptMapGroupUnmappedMode.  # noqa: E501
        :rtype: list[str]
        """
        return self._format_comments_pre

    @format_comments_pre.setter
    def format_comments_pre(self, format_comments_pre):
        """Sets the format_comments_pre of this EnumerationConceptMapGroupUnmappedMode.


        :param format_comments_pre: The format_comments_pre of this EnumerationConceptMapGroupUnmappedMode.  # noqa: E501
        :type: list[str]
        """

        self._format_comments_pre = format_comments_pre

    @property
    def format_comments_post(self):
        """Gets the format_comments_post of this EnumerationConceptMapGroupUnmappedMode.  # noqa: E501


        :return: The format_comments_post of this EnumerationConceptMapGroupUnmappedMode.  # noqa: E501
        :rtype: list[str]
        """
        return self._format_comments_post

    @format_comments_post.setter
    def format_comments_post(self, format_comments_post):
        """Sets the format_comments_post of this EnumerationConceptMapGroupUnmappedMode.


        :param format_comments_post: The format_comments_post of this EnumerationConceptMapGroupUnmappedMode.  # noqa: E501
        :type: list[str]
        """

        self._format_comments_post = format_comments_post

    @property
    def id(self):
        """Gets the id of this EnumerationConceptMapGroupUnmappedMode.  # noqa: E501


        :return: The id of this EnumerationConceptMapGroupUnmappedMode.  # noqa: E501
        :rtype: str
        """
        return self._id

    @id.setter
    def id(self, id):
        """Sets the id of this EnumerationConceptMapGroupUnmappedMode.


        :param id: The id of this EnumerationConceptMapGroupUnmappedMode.  # noqa: E501
        :type: str
        """

        self._id = id

    @property
    def extension(self):
        """Gets the extension of this EnumerationConceptMapGroupUnmappedMode.  # noqa: E501


        :return: The extension of this EnumerationConceptMapGroupUnmappedMode.  # noqa: E501
        :rtype: list[Extension]
        """
        return self._extension

    @extension.setter
    def extension(self, extension):
        """Sets the extension of this EnumerationConceptMapGroupUnmappedMode.


        :param extension: The extension of this EnumerationConceptMapGroupUnmappedMode.  # noqa: E501
        :type: list[Extension]
        """

        self._extension = extension

    @property
    def disallow_extensions(self):
        """Gets the disallow_extensions of this EnumerationConceptMapGroupUnmappedMode.  # noqa: E501


        :return: The disallow_extensions of this EnumerationConceptMapGroupUnmappedMode.  # noqa: E501
        :rtype: bool
        """
        return self._disallow_extensions

    @disallow_extensions.setter
    def disallow_extensions(self, disallow_extensions):
        """Sets the disallow_extensions of this EnumerationConceptMapGroupUnmappedMode.


        :param disallow_extensions: The disallow_extensions of this EnumerationConceptMapGroupUnmappedMode.  # noqa: E501
        :type: bool
        """

        self._disallow_extensions = disallow_extensions

    @property
    def version(self):
        """Gets the version of this EnumerationConceptMapGroupUnmappedMode.  # noqa: E501


        :return: The version of this EnumerationConceptMapGroupUnmappedMode.  # noqa: E501
        :rtype: str
        """
        return self._version

    @version.setter
    def version(self, version):
        """Sets the version of this EnumerationConceptMapGroupUnmappedMode.


        :param version: The version of this EnumerationConceptMapGroupUnmappedMode.  # noqa: E501
        :type: str
        """

        self._version = version

    @property
    def code(self):
        """Gets the code of this EnumerationConceptMapGroupUnmappedMode.  # noqa: E501


        :return: The code of this EnumerationConceptMapGroupUnmappedMode.  # noqa: E501
        :rtype: str
        """
        return self._code

    @code.setter
    def code(self, code):
        """Sets the code of this EnumerationConceptMapGroupUnmappedMode.


        :param code: The code of this EnumerationConceptMapGroupUnmappedMode.  # noqa: E501
        :type: str
        """

        self._code = code

    @property
    def system(self):
        """Gets the system of this EnumerationConceptMapGroupUnmappedMode.  # noqa: E501


        :return: The system of this EnumerationConceptMapGroupUnmappedMode.  # noqa: E501
        :rtype: str
        """
        return self._system

    @system.setter
    def system(self, system):
        """Sets the system of this EnumerationConceptMapGroupUnmappedMode.


        :param system: The system of this EnumerationConceptMapGroupUnmappedMode.  # noqa: E501
        :type: str
        """

        self._system = system

    @property
    def enum_factory(self):
        """Gets the enum_factory of this EnumerationConceptMapGroupUnmappedMode.  # noqa: E501


        :return: The enum_factory of this EnumerationConceptMapGroupUnmappedMode.  # noqa: E501
        :rtype: EnumFactoryConceptMapGroupUnmappedMode
        """
        return self._enum_factory

    @enum_factory.setter
    def enum_factory(self, enum_factory):
        """Sets the enum_factory of this EnumerationConceptMapGroupUnmappedMode.


        :param enum_factory: The enum_factory of this EnumerationConceptMapGroupUnmappedMode.  # noqa: E501
        :type: EnumFactoryConceptMapGroupUnmappedMode
        """

        self._enum_factory = enum_factory

    @property
    def display(self):
        """Gets the display of this EnumerationConceptMapGroupUnmappedMode.  # noqa: E501


        :return: The display of this EnumerationConceptMapGroupUnmappedMode.  # noqa: E501
        :rtype: str
        """
        return self._display

    @display.setter
    def display(self, display):
        """Sets the display of this EnumerationConceptMapGroupUnmappedMode.


        :param display: The display of this EnumerationConceptMapGroupUnmappedMode.  # noqa: E501
        :type: str
        """

        self._display = display

    @property
    def value_as_string(self):
        """Gets the value_as_string of this EnumerationConceptMapGroupUnmappedMode.  # noqa: E501


        :return: The value_as_string of this EnumerationConceptMapGroupUnmappedMode.  # noqa: E501
        :rtype: str
        """
        return self._value_as_string

    @value_as_string.setter
    def value_as_string(self, value_as_string):
        """Sets the value_as_string of this EnumerationConceptMapGroupUnmappedMode.


        :param value_as_string: The value_as_string of this EnumerationConceptMapGroupUnmappedMode.  # noqa: E501
        :type: str
        """

        self._value_as_string = value_as_string

    @property
    def value(self):
        """Gets the value of this EnumerationConceptMapGroupUnmappedMode.  # noqa: E501


        :return: The value of this EnumerationConceptMapGroupUnmappedMode.  # noqa: E501
        :rtype: str
        """
        return self._value

    @value.setter
    def value(self, value):
        """Sets the value of this EnumerationConceptMapGroupUnmappedMode.


        :param value: The value of this EnumerationConceptMapGroupUnmappedMode.  # noqa: E501
        :type: str
        """
        allowed_values = ["PROVIDED", "FIXED", "OTHERMAP", "NULL"]  # noqa: E501
        if value not in allowed_values:
            raise ValueError(
                "Invalid value for `value` ({0}), must be one of {1}"  # noqa: E501
                .format(value, allowed_values)
            )

        self._value = value

    @property
    def empty(self):
        """Gets the empty of this EnumerationConceptMapGroupUnmappedMode.  # noqa: E501


        :return: The empty of this EnumerationConceptMapGroupUnmappedMode.  # noqa: E501
        :rtype: bool
        """
        return self._empty

    @empty.setter
    def empty(self, empty):
        """Sets the empty of this EnumerationConceptMapGroupUnmappedMode.


        :param empty: The empty of this EnumerationConceptMapGroupUnmappedMode.  # noqa: E501
        :type: bool
        """

        self._empty = empty

    @property
    def primitive(self):
        """Gets the primitive of this EnumerationConceptMapGroupUnmappedMode.  # noqa: E501


        :return: The primitive of this EnumerationConceptMapGroupUnmappedMode.  # noqa: E501
        :rtype: bool
        """
        return self._primitive

    @primitive.setter
    def primitive(self, primitive):
        """Sets the primitive of this EnumerationConceptMapGroupUnmappedMode.


        :param primitive: The primitive of this EnumerationConceptMapGroupUnmappedMode.  # noqa: E501
        :type: bool
        """

        self._primitive = primitive

    @property
    def id_element(self):
        """Gets the id_element of this EnumerationConceptMapGroupUnmappedMode.  # noqa: E501


        :return: The id_element of this EnumerationConceptMapGroupUnmappedMode.  # noqa: E501
        :rtype: StringType
        """
        return self._id_element

    @id_element.setter
    def id_element(self, id_element):
        """Sets the id_element of this EnumerationConceptMapGroupUnmappedMode.


        :param id_element: The id_element of this EnumerationConceptMapGroupUnmappedMode.  # noqa: E501
        :type: StringType
        """

        self._id_element = id_element

    @property
    def extension_first_rep(self):
        """Gets the extension_first_rep of this EnumerationConceptMapGroupUnmappedMode.  # noqa: E501


        :return: The extension_first_rep of this EnumerationConceptMapGroupUnmappedMode.  # noqa: E501
        :rtype: Extension
        """
        return self._extension_first_rep

    @extension_first_rep.setter
    def extension_first_rep(self, extension_first_rep):
        """Sets the extension_first_rep of this EnumerationConceptMapGroupUnmappedMode.


        :param extension_first_rep: The extension_first_rep of this EnumerationConceptMapGroupUnmappedMode.  # noqa: E501
        :type: Extension
        """

        self._extension_first_rep = extension_first_rep

    @property
    def id_base(self):
        """Gets the id_base of this EnumerationConceptMapGroupUnmappedMode.  # noqa: E501


        :return: The id_base of this EnumerationConceptMapGroupUnmappedMode.  # noqa: E501
        :rtype: str
        """
        return self._id_base

    @id_base.setter
    def id_base(self, id_base):
        """Sets the id_base of this EnumerationConceptMapGroupUnmappedMode.


        :param id_base: The id_base of this EnumerationConceptMapGroupUnmappedMode.  # noqa: E501
        :type: str
        """

        self._id_base = id_base

    @property
    def resource(self):
        """Gets the resource of this EnumerationConceptMapGroupUnmappedMode.  # noqa: E501


        :return: The resource of this EnumerationConceptMapGroupUnmappedMode.  # noqa: E501
        :rtype: bool
        """
        return self._resource

    @resource.setter
    def resource(self, resource):
        """Sets the resource of this EnumerationConceptMapGroupUnmappedMode.


        :param resource: The resource of this EnumerationConceptMapGroupUnmappedMode.  # noqa: E501
        :type: bool
        """

        self._resource = resource

    @property
    def boolean_primitive(self):
        """Gets the boolean_primitive of this EnumerationConceptMapGroupUnmappedMode.  # noqa: E501


        :return: The boolean_primitive of this EnumerationConceptMapGroupUnmappedMode.  # noqa: E501
        :rtype: bool
        """
        return self._boolean_primitive

    @boolean_primitive.setter
    def boolean_primitive(self, boolean_primitive):
        """Sets the boolean_primitive of this EnumerationConceptMapGroupUnmappedMode.


        :param boolean_primitive: The boolean_primitive of this EnumerationConceptMapGroupUnmappedMode.  # noqa: E501
        :type: bool
        """

        self._boolean_primitive = boolean_primitive

    @property
    def date_time(self):
        """Gets the date_time of this EnumerationConceptMapGroupUnmappedMode.  # noqa: E501


        :return: The date_time of this EnumerationConceptMapGroupUnmappedMode.  # noqa: E501
        :rtype: bool
        """
        return self._date_time

    @date_time.setter
    def date_time(self, date_time):
        """Sets the date_time of this EnumerationConceptMapGroupUnmappedMode.


        :param date_time: The date_time of this EnumerationConceptMapGroupUnmappedMode.  # noqa: E501
        :type: bool
        """

        self._date_time = date_time

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, EnumerationConceptMapGroupUnmappedMode):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        return not self == other
