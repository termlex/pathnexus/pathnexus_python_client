# coding: utf-8

"""
    Pathnexus

    Pathnexus - a lab data harmonisation platform  # noqa: E501

    OpenAPI spec version: 5.8.0
    Contact: support@termlex.com
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""


import pprint
import re  # noqa: F401

import six


class UserDTO(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """

    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'id': 'str',
        'login': 'str',
        'first_name': 'str',
        'last_name': 'str',
        'email': 'str',
        'image_url': 'str',
        'activated': 'bool',
        'lang_key': 'str',
        'created_by': 'str',
        'created_date': 'int',
        'last_modified_by': 'str',
        'last_modified_date': 'int',
        'authorities': 'list[str]'
    }

    attribute_map = {
        'id': 'id',
        'login': 'login',
        'first_name': 'firstName',
        'last_name': 'lastName',
        'email': 'email',
        'image_url': 'imageUrl',
        'activated': 'activated',
        'lang_key': 'langKey',
        'created_by': 'createdBy',
        'created_date': 'createdDate',
        'last_modified_by': 'lastModifiedBy',
        'last_modified_date': 'lastModifiedDate',
        'authorities': 'authorities'
    }

    def __init__(self, id=None, login=None, first_name=None, last_name=None, email=None, image_url=None, activated=None, lang_key=None, created_by=None, created_date=None, last_modified_by=None, last_modified_date=None, authorities=None):  # noqa: E501
        """UserDTO - a model defined in Swagger"""  # noqa: E501

        self._id = None
        self._login = None
        self._first_name = None
        self._last_name = None
        self._email = None
        self._image_url = None
        self._activated = None
        self._lang_key = None
        self._created_by = None
        self._created_date = None
        self._last_modified_by = None
        self._last_modified_date = None
        self._authorities = None
        self.discriminator = None

        if id is not None:
            self.id = id
        if login is not None:
            self.login = login
        if first_name is not None:
            self.first_name = first_name
        if last_name is not None:
            self.last_name = last_name
        if email is not None:
            self.email = email
        if image_url is not None:
            self.image_url = image_url
        if activated is not None:
            self.activated = activated
        if lang_key is not None:
            self.lang_key = lang_key
        if created_by is not None:
            self.created_by = created_by
        if created_date is not None:
            self.created_date = created_date
        if last_modified_by is not None:
            self.last_modified_by = last_modified_by
        if last_modified_date is not None:
            self.last_modified_date = last_modified_date
        if authorities is not None:
            self.authorities = authorities

    @property
    def id(self):
        """Gets the id of this UserDTO.  # noqa: E501


        :return: The id of this UserDTO.  # noqa: E501
        :rtype: str
        """
        return self._id

    @id.setter
    def id(self, id):
        """Sets the id of this UserDTO.


        :param id: The id of this UserDTO.  # noqa: E501
        :type: str
        """

        self._id = id

    @property
    def login(self):
        """Gets the login of this UserDTO.  # noqa: E501


        :return: The login of this UserDTO.  # noqa: E501
        :rtype: str
        """
        return self._login

    @login.setter
    def login(self, login):
        """Sets the login of this UserDTO.


        :param login: The login of this UserDTO.  # noqa: E501
        :type: str
        """
        if login is not None and len(login) > 50:
            raise ValueError("Invalid value for `login`, length must be less than or equal to `50`")  # noqa: E501
        if login is not None and len(login) < 1:
            raise ValueError("Invalid value for `login`, length must be greater than or equal to `1`")  # noqa: E501
        if login is not None and not re.search('^(?>[a-zA-Z0-9!$&*+=?^_`{|}~.-]+@[a-zA-Z0-9-]+(?:\\.[a-zA-Z0-9-]+)*)|(?>[_.@A-Za-z0-9-]+)$', login):  # noqa: E501
            raise ValueError("Invalid value for `login`, must be a follow pattern or equal to `/^(?>[a-zA-Z0-9!$&*+=?^_`{|}~.-]+@[a-zA-Z0-9-]+(?:\\.[a-zA-Z0-9-]+)*)|(?>[_.@A-Za-z0-9-]+)$/`")  # noqa: E501

        self._login = login

    @property
    def first_name(self):
        """Gets the first_name of this UserDTO.  # noqa: E501


        :return: The first_name of this UserDTO.  # noqa: E501
        :rtype: str
        """
        return self._first_name

    @first_name.setter
    def first_name(self, first_name):
        """Sets the first_name of this UserDTO.


        :param first_name: The first_name of this UserDTO.  # noqa: E501
        :type: str
        """
        if first_name is not None and len(first_name) > 50:
            raise ValueError("Invalid value for `first_name`, length must be less than or equal to `50`")  # noqa: E501
        if first_name is not None and len(first_name) < 0:
            raise ValueError("Invalid value for `first_name`, length must be greater than or equal to `0`")  # noqa: E501

        self._first_name = first_name

    @property
    def last_name(self):
        """Gets the last_name of this UserDTO.  # noqa: E501


        :return: The last_name of this UserDTO.  # noqa: E501
        :rtype: str
        """
        return self._last_name

    @last_name.setter
    def last_name(self, last_name):
        """Sets the last_name of this UserDTO.


        :param last_name: The last_name of this UserDTO.  # noqa: E501
        :type: str
        """
        if last_name is not None and len(last_name) > 50:
            raise ValueError("Invalid value for `last_name`, length must be less than or equal to `50`")  # noqa: E501
        if last_name is not None and len(last_name) < 0:
            raise ValueError("Invalid value for `last_name`, length must be greater than or equal to `0`")  # noqa: E501

        self._last_name = last_name

    @property
    def email(self):
        """Gets the email of this UserDTO.  # noqa: E501


        :return: The email of this UserDTO.  # noqa: E501
        :rtype: str
        """
        return self._email

    @email.setter
    def email(self, email):
        """Sets the email of this UserDTO.


        :param email: The email of this UserDTO.  # noqa: E501
        :type: str
        """
        if email is not None and len(email) > 254:
            raise ValueError("Invalid value for `email`, length must be less than or equal to `254`")  # noqa: E501
        if email is not None and len(email) < 5:
            raise ValueError("Invalid value for `email`, length must be greater than or equal to `5`")  # noqa: E501

        self._email = email

    @property
    def image_url(self):
        """Gets the image_url of this UserDTO.  # noqa: E501


        :return: The image_url of this UserDTO.  # noqa: E501
        :rtype: str
        """
        return self._image_url

    @image_url.setter
    def image_url(self, image_url):
        """Sets the image_url of this UserDTO.


        :param image_url: The image_url of this UserDTO.  # noqa: E501
        :type: str
        """
        if image_url is not None and len(image_url) > 256:
            raise ValueError("Invalid value for `image_url`, length must be less than or equal to `256`")  # noqa: E501
        if image_url is not None and len(image_url) < 0:
            raise ValueError("Invalid value for `image_url`, length must be greater than or equal to `0`")  # noqa: E501

        self._image_url = image_url

    @property
    def activated(self):
        """Gets the activated of this UserDTO.  # noqa: E501


        :return: The activated of this UserDTO.  # noqa: E501
        :rtype: bool
        """
        return self._activated

    @activated.setter
    def activated(self, activated):
        """Sets the activated of this UserDTO.


        :param activated: The activated of this UserDTO.  # noqa: E501
        :type: bool
        """

        self._activated = activated

    @property
    def lang_key(self):
        """Gets the lang_key of this UserDTO.  # noqa: E501


        :return: The lang_key of this UserDTO.  # noqa: E501
        :rtype: str
        """
        return self._lang_key

    @lang_key.setter
    def lang_key(self, lang_key):
        """Sets the lang_key of this UserDTO.


        :param lang_key: The lang_key of this UserDTO.  # noqa: E501
        :type: str
        """
        if lang_key is not None and len(lang_key) > 10:
            raise ValueError("Invalid value for `lang_key`, length must be less than or equal to `10`")  # noqa: E501
        if lang_key is not None and len(lang_key) < 2:
            raise ValueError("Invalid value for `lang_key`, length must be greater than or equal to `2`")  # noqa: E501

        self._lang_key = lang_key

    @property
    def created_by(self):
        """Gets the created_by of this UserDTO.  # noqa: E501


        :return: The created_by of this UserDTO.  # noqa: E501
        :rtype: str
        """
        return self._created_by

    @created_by.setter
    def created_by(self, created_by):
        """Sets the created_by of this UserDTO.


        :param created_by: The created_by of this UserDTO.  # noqa: E501
        :type: str
        """

        self._created_by = created_by

    @property
    def created_date(self):
        """Gets the created_date of this UserDTO.  # noqa: E501


        :return: The created_date of this UserDTO.  # noqa: E501
        :rtype: int
        """
        return self._created_date

    @created_date.setter
    def created_date(self, created_date):
        """Sets the created_date of this UserDTO.


        :param created_date: The created_date of this UserDTO.  # noqa: E501
        :type: int
        """

        self._created_date = created_date

    @property
    def last_modified_by(self):
        """Gets the last_modified_by of this UserDTO.  # noqa: E501


        :return: The last_modified_by of this UserDTO.  # noqa: E501
        :rtype: str
        """
        return self._last_modified_by

    @last_modified_by.setter
    def last_modified_by(self, last_modified_by):
        """Sets the last_modified_by of this UserDTO.


        :param last_modified_by: The last_modified_by of this UserDTO.  # noqa: E501
        :type: str
        """

        self._last_modified_by = last_modified_by

    @property
    def last_modified_date(self):
        """Gets the last_modified_date of this UserDTO.  # noqa: E501


        :return: The last_modified_date of this UserDTO.  # noqa: E501
        :rtype: int
        """
        return self._last_modified_date

    @last_modified_date.setter
    def last_modified_date(self, last_modified_date):
        """Sets the last_modified_date of this UserDTO.


        :param last_modified_date: The last_modified_date of this UserDTO.  # noqa: E501
        :type: int
        """

        self._last_modified_date = last_modified_date

    @property
    def authorities(self):
        """Gets the authorities of this UserDTO.  # noqa: E501


        :return: The authorities of this UserDTO.  # noqa: E501
        :rtype: list[str]
        """
        return self._authorities

    @authorities.setter
    def authorities(self, authorities):
        """Sets the authorities of this UserDTO.


        :param authorities: The authorities of this UserDTO.  # noqa: E501
        :type: list[str]
        """

        self._authorities = authorities

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, UserDTO):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        return not self == other
