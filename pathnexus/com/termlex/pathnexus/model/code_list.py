# coding: utf-8

"""
    Pathnexus

    Pathnexus - a lab data harmonisation platform  # noqa: E501

    OpenAPI spec version: 5.8.0
    Contact: support@termlex.com
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""


import pprint
import re  # noqa: F401

import six

from pathnexus.com.termlex.pathnexus.model.code_list_meta_data import CodeListMetaData  # noqa: F401,E501
from pathnexus.com.termlex.pathnexus.model.codelist_metrics import CodelistMetrics  # noqa: F401,E501


class CodeList(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """

    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'created_by': 'str',
        'created_date': 'int',
        'last_modified_by': 'str',
        'last_modified_date': 'int',
        'allowed_groups': 'list[str]',
        'organisation': 'str',
        'id': 'str',
        'name': 'str',
        'category': 'str',
        'speciality': 'str',
        'metadata': 'CodeListMetaData',
        'metrics': 'CodelistMetrics',
        'state': 'str',
        'composite_file': 'bool'
    }

    attribute_map = {
        'created_by': 'createdBy',
        'created_date': 'createdDate',
        'last_modified_by': 'lastModifiedBy',
        'last_modified_date': 'lastModifiedDate',
        'allowed_groups': 'allowedGroups',
        'organisation': 'organisation',
        'id': 'id',
        'name': 'name',
        'category': 'category',
        'speciality': 'speciality',
        'metadata': 'metadata',
        'metrics': 'metrics',
        'state': 'state',
        'composite_file': 'compositeFile'
    }

    def __init__(self, created_by=None, created_date=None, last_modified_by=None, last_modified_date=None, allowed_groups=None, organisation=None, id=None, name=None, category=None, speciality=None, metadata=None, metrics=None, state=None, composite_file=None):  # noqa: E501
        """CodeList - a model defined in Swagger"""  # noqa: E501

        self._created_by = None
        self._created_date = None
        self._last_modified_by = None
        self._last_modified_date = None
        self._allowed_groups = None
        self._organisation = None
        self._id = None
        self._name = None
        self._category = None
        self._speciality = None
        self._metadata = None
        self._metrics = None
        self._state = None
        self._composite_file = None
        self.discriminator = None

        if created_by is not None:
            self.created_by = created_by
        if created_date is not None:
            self.created_date = created_date
        if last_modified_by is not None:
            self.last_modified_by = last_modified_by
        if last_modified_date is not None:
            self.last_modified_date = last_modified_date
        if allowed_groups is not None:
            self.allowed_groups = allowed_groups
        if organisation is not None:
            self.organisation = organisation
        if id is not None:
            self.id = id
        self.name = name
        if category is not None:
            self.category = category
        if speciality is not None:
            self.speciality = speciality
        if metadata is not None:
            self.metadata = metadata
        if metrics is not None:
            self.metrics = metrics
        if state is not None:
            self.state = state
        if composite_file is not None:
            self.composite_file = composite_file

    @property
    def created_by(self):
        """Gets the created_by of this CodeList.  # noqa: E501


        :return: The created_by of this CodeList.  # noqa: E501
        :rtype: str
        """
        return self._created_by

    @created_by.setter
    def created_by(self, created_by):
        """Sets the created_by of this CodeList.


        :param created_by: The created_by of this CodeList.  # noqa: E501
        :type: str
        """

        self._created_by = created_by

    @property
    def created_date(self):
        """Gets the created_date of this CodeList.  # noqa: E501


        :return: The created_date of this CodeList.  # noqa: E501
        :rtype: int
        """
        return self._created_date

    @created_date.setter
    def created_date(self, created_date):
        """Sets the created_date of this CodeList.


        :param created_date: The created_date of this CodeList.  # noqa: E501
        :type: int
        """

        self._created_date = created_date

    @property
    def last_modified_by(self):
        """Gets the last_modified_by of this CodeList.  # noqa: E501


        :return: The last_modified_by of this CodeList.  # noqa: E501
        :rtype: str
        """
        return self._last_modified_by

    @last_modified_by.setter
    def last_modified_by(self, last_modified_by):
        """Sets the last_modified_by of this CodeList.


        :param last_modified_by: The last_modified_by of this CodeList.  # noqa: E501
        :type: str
        """

        self._last_modified_by = last_modified_by

    @property
    def last_modified_date(self):
        """Gets the last_modified_date of this CodeList.  # noqa: E501


        :return: The last_modified_date of this CodeList.  # noqa: E501
        :rtype: int
        """
        return self._last_modified_date

    @last_modified_date.setter
    def last_modified_date(self, last_modified_date):
        """Sets the last_modified_date of this CodeList.


        :param last_modified_date: The last_modified_date of this CodeList.  # noqa: E501
        :type: int
        """

        self._last_modified_date = last_modified_date

    @property
    def allowed_groups(self):
        """Gets the allowed_groups of this CodeList.  # noqa: E501


        :return: The allowed_groups of this CodeList.  # noqa: E501
        :rtype: list[str]
        """
        return self._allowed_groups

    @allowed_groups.setter
    def allowed_groups(self, allowed_groups):
        """Sets the allowed_groups of this CodeList.


        :param allowed_groups: The allowed_groups of this CodeList.  # noqa: E501
        :type: list[str]
        """

        self._allowed_groups = allowed_groups

    @property
    def organisation(self):
        """Gets the organisation of this CodeList.  # noqa: E501


        :return: The organisation of this CodeList.  # noqa: E501
        :rtype: str
        """
        return self._organisation

    @organisation.setter
    def organisation(self, organisation):
        """Sets the organisation of this CodeList.


        :param organisation: The organisation of this CodeList.  # noqa: E501
        :type: str
        """

        self._organisation = organisation

    @property
    def id(self):
        """Gets the id of this CodeList.  # noqa: E501


        :return: The id of this CodeList.  # noqa: E501
        :rtype: str
        """
        return self._id

    @id.setter
    def id(self, id):
        """Sets the id of this CodeList.


        :param id: The id of this CodeList.  # noqa: E501
        :type: str
        """

        self._id = id

    @property
    def name(self):
        """Gets the name of this CodeList.  # noqa: E501


        :return: The name of this CodeList.  # noqa: E501
        :rtype: str
        """
        return self._name

    @name.setter
    def name(self, name):
        """Sets the name of this CodeList.


        :param name: The name of this CodeList.  # noqa: E501
        :type: str
        """
        if name is None:
            raise ValueError("Invalid value for `name`, must not be `None`")  # noqa: E501

        self._name = name

    @property
    def category(self):
        """Gets the category of this CodeList.  # noqa: E501


        :return: The category of this CodeList.  # noqa: E501
        :rtype: str
        """
        return self._category

    @category.setter
    def category(self, category):
        """Sets the category of this CodeList.


        :param category: The category of this CodeList.  # noqa: E501
        :type: str
        """

        self._category = category

    @property
    def speciality(self):
        """Gets the speciality of this CodeList.  # noqa: E501


        :return: The speciality of this CodeList.  # noqa: E501
        :rtype: str
        """
        return self._speciality

    @speciality.setter
    def speciality(self, speciality):
        """Sets the speciality of this CodeList.


        :param speciality: The speciality of this CodeList.  # noqa: E501
        :type: str
        """

        self._speciality = speciality

    @property
    def metadata(self):
        """Gets the metadata of this CodeList.  # noqa: E501


        :return: The metadata of this CodeList.  # noqa: E501
        :rtype: CodeListMetaData
        """
        return self._metadata

    @metadata.setter
    def metadata(self, metadata):
        """Sets the metadata of this CodeList.


        :param metadata: The metadata of this CodeList.  # noqa: E501
        :type: CodeListMetaData
        """

        self._metadata = metadata

    @property
    def metrics(self):
        """Gets the metrics of this CodeList.  # noqa: E501


        :return: The metrics of this CodeList.  # noqa: E501
        :rtype: CodelistMetrics
        """
        return self._metrics

    @metrics.setter
    def metrics(self, metrics):
        """Sets the metrics of this CodeList.


        :param metrics: The metrics of this CodeList.  # noqa: E501
        :type: CodelistMetrics
        """

        self._metrics = metrics

    @property
    def state(self):
        """Gets the state of this CodeList.  # noqa: E501


        :return: The state of this CodeList.  # noqa: E501
        :rtype: str
        """
        return self._state

    @state.setter
    def state(self, state):
        """Sets the state of this CodeList.


        :param state: The state of this CodeList.  # noqa: E501
        :type: str
        """

        self._state = state

    @property
    def composite_file(self):
        """Gets the composite_file of this CodeList.  # noqa: E501


        :return: The composite_file of this CodeList.  # noqa: E501
        :rtype: bool
        """
        return self._composite_file

    @composite_file.setter
    def composite_file(self, composite_file):
        """Sets the composite_file of this CodeList.


        :param composite_file: The composite_file of this CodeList.  # noqa: E501
        :type: bool
        """

        self._composite_file = composite_file

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, CodeList):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        return not self == other
