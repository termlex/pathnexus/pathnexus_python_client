# coding: utf-8

"""
    Pathnexus

    Pathnexus - a lab data harmonisation platform  # noqa: E501

    OpenAPI spec version: 5.8.0
    Contact: support@termlex.com
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""


import pprint
import re  # noqa: F401

import six

from pathnexus.com.termlex.pathnexus.model.i_base_meta_type import IBaseMetaType  # noqa: F401,E501
from pathnexus.com.termlex.pathnexus.model.i_id_type import IIdType  # noqa: F401,E501


class IBaseResource(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """

    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'meta': 'IBaseMetaType',
        'id_element': 'IIdType',
        'structure_fhir_version_enum': 'str',
        'format_comments_pre': 'list[str]',
        'format_comments_post': 'list[str]',
        'empty': 'bool'
    }

    attribute_map = {
        'meta': 'meta',
        'id_element': 'idElement',
        'structure_fhir_version_enum': 'structureFhirVersionEnum',
        'format_comments_pre': 'formatCommentsPre',
        'format_comments_post': 'formatCommentsPost',
        'empty': 'empty'
    }

    def __init__(self, meta=None, id_element=None, structure_fhir_version_enum=None, format_comments_pre=None, format_comments_post=None, empty=None):  # noqa: E501
        """IBaseResource - a model defined in Swagger"""  # noqa: E501

        self._meta = None
        self._id_element = None
        self._structure_fhir_version_enum = None
        self._format_comments_pre = None
        self._format_comments_post = None
        self._empty = None
        self.discriminator = None

        if meta is not None:
            self.meta = meta
        if id_element is not None:
            self.id_element = id_element
        if structure_fhir_version_enum is not None:
            self.structure_fhir_version_enum = structure_fhir_version_enum
        if format_comments_pre is not None:
            self.format_comments_pre = format_comments_pre
        if format_comments_post is not None:
            self.format_comments_post = format_comments_post
        if empty is not None:
            self.empty = empty

    @property
    def meta(self):
        """Gets the meta of this IBaseResource.  # noqa: E501


        :return: The meta of this IBaseResource.  # noqa: E501
        :rtype: IBaseMetaType
        """
        return self._meta

    @meta.setter
    def meta(self, meta):
        """Sets the meta of this IBaseResource.


        :param meta: The meta of this IBaseResource.  # noqa: E501
        :type: IBaseMetaType
        """

        self._meta = meta

    @property
    def id_element(self):
        """Gets the id_element of this IBaseResource.  # noqa: E501


        :return: The id_element of this IBaseResource.  # noqa: E501
        :rtype: IIdType
        """
        return self._id_element

    @id_element.setter
    def id_element(self, id_element):
        """Sets the id_element of this IBaseResource.


        :param id_element: The id_element of this IBaseResource.  # noqa: E501
        :type: IIdType
        """

        self._id_element = id_element

    @property
    def structure_fhir_version_enum(self):
        """Gets the structure_fhir_version_enum of this IBaseResource.  # noqa: E501


        :return: The structure_fhir_version_enum of this IBaseResource.  # noqa: E501
        :rtype: str
        """
        return self._structure_fhir_version_enum

    @structure_fhir_version_enum.setter
    def structure_fhir_version_enum(self, structure_fhir_version_enum):
        """Sets the structure_fhir_version_enum of this IBaseResource.


        :param structure_fhir_version_enum: The structure_fhir_version_enum of this IBaseResource.  # noqa: E501
        :type: str
        """
        allowed_values = ["DSTU2", "DSTU2_HL7ORG", "DSTU2_1", "DSTU3", "R4", "R5"]  # noqa: E501
        if structure_fhir_version_enum not in allowed_values:
            raise ValueError(
                "Invalid value for `structure_fhir_version_enum` ({0}), must be one of {1}"  # noqa: E501
                .format(structure_fhir_version_enum, allowed_values)
            )

        self._structure_fhir_version_enum = structure_fhir_version_enum

    @property
    def format_comments_pre(self):
        """Gets the format_comments_pre of this IBaseResource.  # noqa: E501


        :return: The format_comments_pre of this IBaseResource.  # noqa: E501
        :rtype: list[str]
        """
        return self._format_comments_pre

    @format_comments_pre.setter
    def format_comments_pre(self, format_comments_pre):
        """Sets the format_comments_pre of this IBaseResource.


        :param format_comments_pre: The format_comments_pre of this IBaseResource.  # noqa: E501
        :type: list[str]
        """

        self._format_comments_pre = format_comments_pre

    @property
    def format_comments_post(self):
        """Gets the format_comments_post of this IBaseResource.  # noqa: E501


        :return: The format_comments_post of this IBaseResource.  # noqa: E501
        :rtype: list[str]
        """
        return self._format_comments_post

    @format_comments_post.setter
    def format_comments_post(self, format_comments_post):
        """Sets the format_comments_post of this IBaseResource.


        :param format_comments_post: The format_comments_post of this IBaseResource.  # noqa: E501
        :type: list[str]
        """

        self._format_comments_post = format_comments_post

    @property
    def empty(self):
        """Gets the empty of this IBaseResource.  # noqa: E501


        :return: The empty of this IBaseResource.  # noqa: E501
        :rtype: bool
        """
        return self._empty

    @empty.setter
    def empty(self, empty):
        """Sets the empty of this IBaseResource.


        :param empty: The empty of this IBaseResource.  # noqa: E501
        :type: bool
        """

        self._empty = empty

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, IBaseResource):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        return not self == other
