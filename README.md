# pathnexus
Pathnexus - a lab data harmonisation platform

This Python package is the client for Pathnexus - a lab data harmonisation platform

> Please follow the [Getting Started](https://docs.pathnexus.com/getting-started) section for a more complete example

- API version: 5.8.0
- Package version: 5.8.0

## Support
For more information, please visit [https://termlex.com/contact-us](https://termlex.com/contact-us)
or send us an email at:
support@termlex.com


## Requirements.

Python 2.7 and 3.4+

## Installation & Usage
### pip install

You can install directly from the Gitlab repo

```sh
pip install git+https://gitlab.com/termlex/pathnexus/pathnexus_python_client.git
```
(you may need to run `pip` with root permission: `sudo pip install git+https://gitlab.com/termlex/pathnexus/pathnexus_python_client.git`)

Then import the package:
```python
import pathnexus
```

### Setuptools

Install via [Setuptools](http://pypi.python.org/pypi/setuptools).

```sh
python setup.py install --user
```
(or `sudo python setup.py install` to install the package for all users)

Then import the package:
```python
import pathnexus
```

## Getting Started

> Please follow the [Getting Started](https://docs.pathnexus.com/getting-started) section for a more complete example

Please follow the [installation procedure](#installation--usage) and then run the following:

```python
from __future__ import print_function
import time
import pathnexus
from pathnexus.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = pathnexus.DefaultApi(pathnexus.ApiClient(configuration))
body = pathnexus.QueryModel() # QueryModel |  (optional)

try:
    api_instance.bulk_delete_local_code_items(body=body)
except ApiException as e:
    print("Exception when calling DefaultApi->bulk_delete_local_code_items: %s\n" % e)

```

## User Accounts & API Authorization

In order to access Pathnexus, you must first register for an account. Here are the steps for registering for an account

- Go to https://id.termlex.com
- Click on `Signup` link and follow the instructions
- Wait for your account to be approved
- If you have waited for over a week and have not heard back, please [contact us](https://termlex.com/contact)

### Using API Token

In order to access Termnexus API using the client library or any other programmatic route, you need to first retrieve your API token.
The following steps describe how you can retrieve your `API token` using your `username` and `password`

 All endpoints do not require authorization.


## Documentation for API Endpoints

All URIs are relative to *http://https://pathnexus.com/api*

Class | Method | HTTP request | Description
------------ | ------------- | ------------- | -------------
*DefaultApi* | [**bulk_delete_local_code_items**](docs/DefaultApi.md#bulk_delete_local_code_items) | **POST** /api/_bulk-delete/local-code-items | 
*DefaultApi* | [**clear_cache**](docs/DefaultApi.md#clear_cache) | **DELETE** /api/_cache/code-lists | 
*DefaultApi* | [**create_code_list**](docs/DefaultApi.md#create_code_list) | **POST** /api/code-lists | 
*DefaultApi* | [**create_local_code_item**](docs/DefaultApi.md#create_local_code_item) | **POST** /api/local-code-items | 
*DefaultApi* | [**create_map_association**](docs/DefaultApi.md#create_map_association) | **POST** /api/map-associations | 
*DefaultApi* | [**create_master_list_entry**](docs/DefaultApi.md#create_master_list_entry) | **POST** /api/master-list-entries | 
*DefaultApi* | [**delete_code_list**](docs/DefaultApi.md#delete_code_list) | **DELETE** /api/code-lists/{id} | 
*DefaultApi* | [**delete_local_code_item**](docs/DefaultApi.md#delete_local_code_item) | **DELETE** /api/local-code-items/{id} | 
*DefaultApi* | [**delete_map_association**](docs/DefaultApi.md#delete_map_association) | **DELETE** /api/map-associations/{id} | 
*DefaultApi* | [**delete_map_associations_by_query**](docs/DefaultApi.md#delete_map_associations_by_query) | **POST** /api/_delete/map-associations | 
*DefaultApi* | [**delete_master_list_entry**](docs/DefaultApi.md#delete_master_list_entry) | **DELETE** /api/master-list-entries/{id} | 
*DefaultApi* | [**export_code_list_by_query**](docs/DefaultApi.md#export_code_list_by_query) | **POST** /api/_export/code-lists | 
*DefaultApi* | [**export_mappings_for_local_code_items_by_query**](docs/DefaultApi.md#export_mappings_for_local_code_items_by_query) | **POST** /api/_export/local-code-items | 
*DefaultApi* | [**generate_master_list_entries**](docs/DefaultApi.md#generate_master_list_entries) | **POST** /api/_generate/master-list-entries | 
*DefaultApi* | [**get_all_code_lists**](docs/DefaultApi.md#get_all_code_lists) | **GET** /api/code-lists | 
*DefaultApi* | [**get_all_local_code_items**](docs/DefaultApi.md#get_all_local_code_items) | **GET** /api/local-code-items | 
*DefaultApi* | [**get_all_map_associations**](docs/DefaultApi.md#get_all_map_associations) | **GET** /api/map-associations | 
*DefaultApi* | [**get_all_master_list_entries**](docs/DefaultApi.md#get_all_master_list_entries) | **GET** /api/master-list-entries | 
*DefaultApi* | [**get_all_users**](docs/DefaultApi.md#get_all_users) | **GET** /api/users | 
*DefaultApi* | [**get_associations_with_given_status_for_code_list**](docs/DefaultApi.md#get_associations_with_given_status_for_code_list) | **GET** /api/code-lists/{id}/map-associations | 
*DefaultApi* | [**get_associations_with_given_status_for_local_code_item**](docs/DefaultApi.md#get_associations_with_given_status_for_local_code_item) | **GET** /api/local-code-items/{id}/map-associations | 
*DefaultApi* | [**get_authorities**](docs/DefaultApi.md#get_authorities) | **GET** /api/users/authorities | 
*DefaultApi* | [**get_code_list**](docs/DefaultApi.md#get_code_list) | **GET** /api/code-lists/{id} | 
*DefaultApi* | [**get_code_list_as_fhir**](docs/DefaultApi.md#get_code_list_as_fhir) | **GET** /api/code-lists/{id}/fhir | 
*DefaultApi* | [**get_code_list_history**](docs/DefaultApi.md#get_code_list_history) | **GET** /api/code-lists/{id}/history | 
*DefaultApi* | [**get_local_code_item**](docs/DefaultApi.md#get_local_code_item) | **GET** /api/local-code-items/{id} | 
*DefaultApi* | [**get_local_code_items_for_code_list**](docs/DefaultApi.md#get_local_code_items_for_code_list) | **GET** /api/code-lists/{id}/local-code-items | 
*DefaultApi* | [**get_map_association**](docs/DefaultApi.md#get_map_association) | **GET** /api/map-associations/{id} | 
*DefaultApi* | [**get_master_list_entry**](docs/DefaultApi.md#get_master_list_entry) | **GET** /api/master-list-entries/{id} | 
*DefaultApi* | [**get_metadata_by_code_list_id**](docs/DefaultApi.md#get_metadata_by_code_list_id) | **GET** /api/code-lists/{id}/metadata | 
*DefaultApi* | [**get_suggested_matches**](docs/DefaultApi.md#get_suggested_matches) | **POST** /api/local-code-items/{id}/matches | 
*DefaultApi* | [**get_user**](docs/DefaultApi.md#get_user) | **GET** /api/users/{login} | 
*DefaultApi* | [**import_file**](docs/DefaultApi.md#import_file) | **POST** /api/code-lists/{id}/_import/local-code-items | 
*DefaultApi* | [**index_code_lists**](docs/DefaultApi.md#index_code_lists) | **PUT** /api/_index/code-lists | 
*DefaultApi* | [**index_local_code_items**](docs/DefaultApi.md#index_local_code_items) | **PUT** /api/_index/local-code-items | 
*DefaultApi* | [**index_map_associations**](docs/DefaultApi.md#index_map_associations) | **PUT** /api/_index/map-associations | 
*DefaultApi* | [**index_master_list_entries**](docs/DefaultApi.md#index_master_list_entries) | **PUT** /api/_index/master-list-entries | 
*DefaultApi* | [**purge_orphan**](docs/DefaultApi.md#purge_orphan) | **DELETE** /api/_purge/orphans | 
*DefaultApi* | [**retrieve_code_list_from_stash**](docs/DefaultApi.md#retrieve_code_list_from_stash) | **GET** /api/_stash/code-lists/{id} | 
*DefaultApi* | [**search**](docs/DefaultApi.md#search) | **GET** /api/_search/users/{query} | 
*DefaultApi* | [**search_code_lists**](docs/DefaultApi.md#search_code_lists) | **POST** /api/_search/code-lists | 
*DefaultApi* | [**search_local_code_items**](docs/DefaultApi.md#search_local_code_items) | **POST** /api/_search/local-code-items | 
*DefaultApi* | [**search_map_associations**](docs/DefaultApi.md#search_map_associations) | **POST** /api/_search/map-associations | 
*DefaultApi* | [**search_master_list_entries**](docs/DefaultApi.md#search_master_list_entries) | **POST** /api/_search/master-list-entries | 
*DefaultApi* | [**statsh_code_list**](docs/DefaultApi.md#statsh_code_list) | **PUT** /api/_stash/code-lists/{id} | 
*DefaultApi* | [**tag_local_code_items_by_query**](docs/DefaultApi.md#tag_local_code_items_by_query) | **POST** /api/_tag/local-code-items | 
*DefaultApi* | [**update_bulk_audit**](docs/DefaultApi.md#update_bulk_audit) | **POST** /api/_bulk-audit-update/map-association | 
*DefaultApi* | [**update_code_list**](docs/DefaultApi.md#update_code_list) | **PUT** /api/code-lists | 
*DefaultApi* | [**update_groups_for_code_lists_by_query**](docs/DefaultApi.md#update_groups_for_code_lists_by_query) | **PUT** /api/_groups/code-lists | 
*DefaultApi* | [**update_local_code_item**](docs/DefaultApi.md#update_local_code_item) | **PUT** /api/local-code-items | 
*DefaultApi* | [**update_map_association**](docs/DefaultApi.md#update_map_association) | **PUT** /api/map-associations | 
*DefaultApi* | [**update_master_list_entry**](docs/DefaultApi.md#update_master_list_entry) | **PUT** /api/master-list-entries | 


## Documentation For Models

 - [Address](docs/Address.md)
 - [BooleanType](docs/BooleanType.md)
 - [CanonicalType](docs/CanonicalType.md)
 - [CategoryItem](docs/CategoryItem.md)
 - [Change](docs/Change.md)
 - [CodeList](docs/CodeList.md)
 - [CodeListMetaData](docs/CodeListMetaData.md)
 - [CodeType](docs/CodeType.md)
 - [CodeableConcept](docs/CodeableConcept.md)
 - [CodelistMetrics](docs/CodelistMetrics.md)
 - [Coding](docs/Coding.md)
 - [ColumnMetadata](docs/ColumnMetadata.md)
 - [CommitId](docs/CommitId.md)
 - [CommitMetadata](docs/CommitMetadata.md)
 - [ConceptMap](docs/ConceptMap.md)
 - [ConceptMapGroupComponent](docs/ConceptMapGroupComponent.md)
 - [ConceptMapGroupUnmappedComponent](docs/ConceptMapGroupUnmappedComponent.md)
 - [ConceptMini](docs/ConceptMini.md)
 - [ContactDetail](docs/ContactDetail.md)
 - [ContactPoint](docs/ContactPoint.md)
 - [DateTimeType](docs/DateTimeType.md)
 - [DecimalType](docs/DecimalType.md)
 - [Endpoint](docs/Endpoint.md)
 - [Enum](docs/Enum.md)
 - [EnumFactory](docs/EnumFactory.md)
 - [EnumFactoryAddressType](docs/EnumFactoryAddressType.md)
 - [EnumFactoryAddressUse](docs/EnumFactoryAddressUse.md)
 - [EnumFactoryConceptMapEquivalence](docs/EnumFactoryConceptMapEquivalence.md)
 - [EnumFactoryConceptMapGroupUnmappedMode](docs/EnumFactoryConceptMapGroupUnmappedMode.md)
 - [EnumFactoryContactPointSystem](docs/EnumFactoryContactPointSystem.md)
 - [EnumFactoryContactPointUse](docs/EnumFactoryContactPointUse.md)
 - [EnumFactoryEndpointStatus](docs/EnumFactoryEndpointStatus.md)
 - [EnumFactoryEnum](docs/EnumFactoryEnum.md)
 - [EnumFactoryIdentifierUse](docs/EnumFactoryIdentifierUse.md)
 - [EnumFactoryNameUse](docs/EnumFactoryNameUse.md)
 - [EnumFactoryNarrativeStatus](docs/EnumFactoryNarrativeStatus.md)
 - [EnumFactoryPublicationStatus](docs/EnumFactoryPublicationStatus.md)
 - [EnumFactoryQuantityComparator](docs/EnumFactoryQuantityComparator.md)
 - [Enumeration](docs/Enumeration.md)
 - [EnumerationAddressType](docs/EnumerationAddressType.md)
 - [EnumerationAddressUse](docs/EnumerationAddressUse.md)
 - [EnumerationConceptMapEquivalence](docs/EnumerationConceptMapEquivalence.md)
 - [EnumerationConceptMapGroupUnmappedMode](docs/EnumerationConceptMapGroupUnmappedMode.md)
 - [EnumerationContactPointSystem](docs/EnumerationContactPointSystem.md)
 - [EnumerationContactPointUse](docs/EnumerationContactPointUse.md)
 - [EnumerationEndpointStatus](docs/EnumerationEndpointStatus.md)
 - [EnumerationIdentifierUse](docs/EnumerationIdentifierUse.md)
 - [EnumerationNameUse](docs/EnumerationNameUse.md)
 - [EnumerationNarrativeStatus](docs/EnumerationNarrativeStatus.md)
 - [EnumerationPublicationStatus](docs/EnumerationPublicationStatus.md)
 - [EnumerationQuantityComparator](docs/EnumerationQuantityComparator.md)
 - [Extension](docs/Extension.md)
 - [GlobalId](docs/GlobalId.md)
 - [HumanName](docs/HumanName.md)
 - [IBaseCoding](docs/IBaseCoding.md)
 - [IBaseMetaType](docs/IBaseMetaType.md)
 - [IBaseResource](docs/IBaseResource.md)
 - [IIdType](docs/IIdType.md)
 - [IPrimitiveType](docs/IPrimitiveType.md)
 - [IPrimitiveTypeObject](docs/IPrimitiveTypeObject.md)
 - [IPrimitiveTypeString](docs/IPrimitiveTypeString.md)
 - [IdType](docs/IdType.md)
 - [Identifier](docs/Identifier.md)
 - [InstantType](docs/InstantType.md)
 - [LocalCodeItem](docs/LocalCodeItem.md)
 - [Location](docs/Location.md)
 - [MapAssociation](docs/MapAssociation.md)
 - [MarkdownType](docs/MarkdownType.md)
 - [MasterListEntry](docs/MasterListEntry.md)
 - [Meta](docs/Meta.md)
 - [Narrative](docs/Narrative.md)
 - [Number](docs/Number.md)
 - [Organization](docs/Organization.md)
 - [OrganizationContactComponent](docs/OrganizationContactComponent.md)
 - [OtherElementComponent](docs/OtherElementComponent.md)
 - [Page](docs/Page.md)
 - [PageLocalCodeItem](docs/PageLocalCodeItem.md)
 - [Pageable](docs/Pageable.md)
 - [PathnexusQueryModel](docs/PathnexusQueryModel.md)
 - [Period](docs/Period.md)
 - [PositiveIntType](docs/PositiveIntType.md)
 - [Quantity](docs/Quantity.md)
 - [QueryModel](docs/QueryModel.md)
 - [Range](docs/Range.md)
 - [Reference](docs/Reference.md)
 - [Resource](docs/Resource.md)
 - [SearchCategory](docs/SearchCategory.md)
 - [SearchResults](docs/SearchResults.md)
 - [Sort](docs/Sort.md)
 - [SourceElementComponent](docs/SourceElementComponent.md)
 - [StringType](docs/StringType.md)
 - [TargetElementComponent](docs/TargetElementComponent.md)
 - [TermLangPojo](docs/TermLangPojo.md)
 - [TimeZone](docs/TimeZone.md)
 - [Type](docs/Type.md)
 - [UriType](docs/UriType.md)
 - [UrlType](docs/UrlType.md)
 - [UsageContext](docs/UsageContext.md)
 - [User](docs/User.md)
 - [UserDTO](docs/UserDTO.md)
 - [XhtmlNode](docs/XhtmlNode.md)


